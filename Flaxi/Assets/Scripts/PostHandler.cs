﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data; 
using System;
using UnityEngine.UI;
using TMPro;

public class PostHandler : MonoBehaviour
{
    public GameObject refObj;
    public string conn;
    public int page = 1;
    public int q = 3; //quantity (how many items on a page)
    public GameObject entries;
    public GameObject refInput;
    public GameObject refCalendar;
    public GameObject refTrainingOutput;

    public GameObject search;
    public GameObject searchTextbox;
    public GameObject checkboxPanel;
    public GameObject searchDropdown;
    public void Start () 
    {
        // search = refObj.transform.GetComponent<References>().search;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////  D A T A B A S E     I N     P R O G R E S S ////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        refObj.transform.GetComponent<References>().dbInitConnect();
    }

    public void doActionPostDiary()
    {
        // Cu cele 2 comenzi de mai joc vedem ce facem.
        // Poate il lasam sa mai posteze ceva? Sau ii afisam un mesaj de confirmare dupa ce il scoatem?
        refObj.transform.GetComponent<References>().main.SetActive(true);
        refObj.transform.GetComponent<References>().diary.SetActive(false);

        // Aici e facut cu GetChild si numarul lui. Poate schimb sa caute exact elementul dorit
        string title = refInput.transform.GetComponent<Input_References>().diary_title.GetComponent<TMP_InputField>().text; 
        string bookmark = "0";
        string date = System.DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
        string text = refInput.transform.GetComponent<Input_References>().diary_text.GetComponent<TMP_InputField>().text;

        //escape quote marks
        title = title.Replace("'", "''");
        text = text.Replace("'", "''");
        string query = "INSERT INTO diary (date, bookmark, title, text) VALUES (\"" + date + "\",\"" + bookmark + "\",'" + title + "','" + text + "');";
        refObj.transform.GetComponent<References>().executeNonQuery(query);
    }

    public void Do_001_Action_Post_Task()
    {
        Debug.Log("Task Posted With Success");
        string title = refInput.transform.GetComponent<Input_References>().new_task_input_task_name.transform.GetComponent<TMP_InputField>().text; ;
        string deadline = refCalendar.transform.GetComponent<Calendar_References>().calendar_array[4].transform.GetComponent<TMP_Text>().text;

        string query = "INSERT INTO tasks (title, deadline)" +
                       " VALUES ('" + title + "','" + deadline +  "');";
        //Debug.Log(query);
        refObj.transform.GetComponent<References>().executeNonQuery(query);
        refObj.transform.GetComponent<References>().counter = -1;
        refObj.transform.GetComponent<References>().Message = " New task added!   Wish you all the best! ";
    }

    public void Do_002_Action_Post_Skill()
    {
        Debug.Log("New Skill Aded With Success");
        string skill_name = refInput.transform.GetComponent<Input_References>()
                            .new_skill_input_skill_name.transform.GetComponent<TMP_InputField>().text;
                
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //// Aici schimb dropdownul de la value in text. N-am gasit comanda sau iau direct ///////////////////////////////////////
        
        string[] skill_sevel_list = new string[] { "None", "Novice", "Intermediate", "Advanced", "Expert", "Master" };
        int skill_level_int = refInput.transform.GetComponent<Input_References>()
                            .new_skill_dropwodn_skill_level.transform.GetComponent<TMP_Dropdown>().value;
        string skill_level = skill_sevel_list[skill_level_int];

        string hours_invested = refInput.transform.GetComponent<Input_References>()
                                .new_skill_input_skill_total_time.transform.GetComponent<TMP_InputField>().text;

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //// Aici schimb data intr-un singur text sa il pot da frumos bazei de date ///////////////////////////////////////
        string started_learning = refCalendar.transform.GetComponent<Calendar_References>()
                        .calendar_array[5].transform.GetComponent<TMP_Text>().text;

        // la data trebuie sa avem in vedere ca este obligatoriu sa fie totul sub forma YYYY-MM-DD. de ex 09-2-2 nu ar merge. O sa mai fac
        // o functie in care sa inlocuiesc 08 la an cu 2008 si la luna sau zi daca pun 3 sa transforme automat in 03. Nu-i mare lucru
        // poate o sa pun sa apara si cateva erori daca nu sunt puse toate valorile sau daca ies din parametrii da asta mai tarziu.
        Debug.Log(started_learning);


        //// Aici schimb data intr-un singur text sa il pot da frumos bazei de date ///////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        string enjoyment_level = refInput.transform.GetComponent<Input_References>()
                                .new_skill_input_enjoyment.transform.GetComponent<TMP_InputField>().text;

        // daca foloseste semne gen " '  "  sau " " " banuiesc ca o sa dea eroare. Da tinand cont de faptul ca nu mai e diary o sa am grija sa se interzica
        // semnele speciale. Sau o sa fac asta din Unity daca ma lasa sau mai adaug un mic script care sau da eroare sau scoate direct caracterele
        string query = "INSERT INTO skills(skill_name, skill_level, hours_invested, started_learning, enjoyment_level, place_order)" +
                      "VALUES ( '" + skill_name + "', '" + skill_level + "', " + hours_invested + ", '" + started_learning +"', " + enjoyment_level + ", " + "(SELECT IFNULL(((SELECT MAX(id) FROM skills)), 0))" + ");";

        Debug.Log(query);
        refObj.transform.GetComponent<References>().executeNonQuery(query);

        //transform.parent.parent.GetChild(1).gameObject.SetActive(true);
        //transform.parent.gameObject.SetActive(false);
    }

    public void Do_003_Action_Post_Job()
    {
        Debug.Log("New Job Aded With Success");
        string job_name = refInput.transform.GetComponent<Input_References>().new_job_name.transform.GetComponent<TMP_InputField>().text;
        
        bool[] d = new bool[8];
        for(int i=1; i<=7; i++)
            d[i] = refInput.transform.GetComponent<Input_References>().new_job_d[i].transform.GetComponent<Toggle>().isOn;

        string daily_hours = refInput.transform.GetComponent<Input_References>()
                            .new_job_daily_hours.transform.GetComponent<TMP_InputField>().text;

        string s_year = refInput.transform.GetComponent<Input_References>().new_job_start_year.transform.GetComponent<TMP_InputField>().text;
        string s_month = refInput.transform.GetComponent<Input_References>().new_job_start_month.transform.GetComponent<TMP_InputField>().text;
        string s_day = refInput.transform.GetComponent<Input_References>().new_job_start_day.transform.GetComponent<TMP_InputField>().text;

        string start_date = s_year + "-" + s_month + "-" + s_day;
        Debug.Log(start_date);

        string vacation_days_year = refInput.transform.GetComponent<Input_References>()
                                    .new_job_vacation_days.transform.GetComponent<TMP_InputField>().text;

        string anual_bonus = refInput.transform.GetComponent<Input_References>()
                                    .new_job_anual_bonus.transform.GetComponent<TMP_InputField>().text;

        string monthly_salary = refInput.transform.GetComponent<Input_References>()
                                .new_job_salary_amount.transform.GetComponent<TMP_InputField>().text;

        string job_satisfaction = refInput.transform.GetComponent<Input_References>()
                                .new_job_enjoyment.transform.GetComponent<TMP_InputField>().text;

        bool is_main = refInput.transform.GetComponent<Input_References>().new_job_is_main.transform.GetComponent<Toggle>().isOn;

        string query = "INSERT INTO income(job_name, d_1, d_2, d_3, d_4, d_5, d_6, d_7, daily_hours," +
                        "start_date,vacation_days_year, anual_bonus, monthly_salary, job_satisfaction, is_main)" +
                       "VALUES ('" + job_name + "', " + d[1] + ", "+ d[2] + ", " + d[3] + ", " + d[4] + ", " + d[5] + ", " + d[6] + ", " + d[7] + ", " 
                       + daily_hours + ", '" + start_date + "', " + vacation_days_year + ", " + anual_bonus + ", " + monthly_salary + ", " 
                       + job_satisfaction + ", " + is_main +  ");";
        Debug.Log(query);
        refObj.transform.GetComponent<References>().executeNonQuery(query);

        //transform.parent.parent.GetChild(1).gameObject.SetActive(true);
        //transform.parent.gameObject.SetActive(false);
    }

    public void Do_003_Action_Post_New_Person()
    {
        Debug.Log("New Person Aded With Success");

        string first_name = refInput.transform.GetComponent<Input_References>()
                            .new_person_first_name.transform.GetComponent<TMP_InputField>().text;
        string last_name = refInput.transform.GetComponent<Input_References>()
                            .new_person_last_name.transform.GetComponent<TMP_InputField>().text;
        string[] status_list = new string[] { "Family", "Friend"};
        int status_int = refInput.transform.GetComponent<Input_References>()
                            .new_person_status_int.transform.GetComponent<TMP_Dropdown>().value;
        string person_status = status_list[status_int];
        string status_name = refInput.transform.GetComponent<Input_References>()
                            .new_person_status_name.transform.GetComponent<TMP_InputField>().text;
        string birth_date = refCalendar.transform.GetComponent<Calendar_References>()
                            .calendar_array[0].transform.GetComponent<TMP_Text>().text;
        string friends_since = refCalendar.transform.GetComponent<Calendar_References>()
                            .calendar_array[1].transform.GetComponent<TMP_Text>().text;
        bool bookmarked = refInput.transform.GetComponent<Input_References>()
                         .new_person_bookmarked.transform.GetComponent<Toggle>().isOn;
        string favourite_colour = refInput.transform.GetComponent<Input_References>()
                          .new_person_favourite_colour.transform.GetComponent<TMP_InputField>().text;
        string job_name = refInput.transform.GetComponent<Input_References>()
                          .new_person_job_name.transform.GetComponent<TMP_InputField>().text;

        string query = "INSERT INTO family(first_name, last_name, person_status, status_name, birth_date, friends_since, bookmarked, favourite_colour, job_name)" +
                       "VALUES ('" + first_name + "', '" + last_name +"', '" + person_status + "', '" + status_name + "', '" + birth_date + "', '" +
                       friends_since + "', "+ bookmarked + ", '"+ favourite_colour + "', '" + job_name +"'" + ");";
        Debug.Log(query);
        refObj.transform.GetComponent<References>().executeNonQuery(query);

        //transform.parent.parent.GetChild(1).gameObject.SetActive(true);
        //transform.parent.gameObject.SetActive(false);
    }

    public void Do_004_Action_Post_New_Exercise()
    {
        string exercise = refInput.transform.GetComponent<Input_References>()
                        .new_exercise_exercise.transform.GetComponent<TMP_InputField>().text;
        string calories_burned_exercise = refInput.transform.GetComponent<Input_References>()
                                        .new_exercise_calories_burned_exercise.transform.GetComponent<TMP_InputField>().text;
        string enjoyment_level = refInput.transform.GetComponent<Input_References>()
                               .new_exercise_enjoyment_level.transform.GetComponent<TMP_InputField>().text;
        string[] difficulty_list = new string[] { "None", "Low", "Medium" ,"High", "Extreme" };
        int difficulty_int = refInput.transform.GetComponent<Input_References>()
                            .new_exercise_difficulty_int.transform.GetComponent<TMP_Dropdown>().value;
        string difficulty = difficulty_list[difficulty_int];

        string query = "INSERT INTO training(exercise, calories_burned_exercise, enjoyment_level, difficulty, place_order)" +
                       "VALUES ('" + exercise + "', " + calories_burned_exercise + ", " +
                       enjoyment_level + ", '" + difficulty + "', " + "(SELECT IFNULL(((SELECT MAX(id) FROM training)), 0))" + ");";
        Debug.Log(query);
        refObj.transform.GetComponent<References>().executeNonQuery(query);
    }

    ///////////////////////////////////////////////////////////////////////
    ///    A I C I    E    F U N C T I A    D  E   S E A R C H ////////////
    ///////////////////////////////////////////////////////////////////////
    public void searchInDB()
    {
        string searchTerm = searchTextbox.transform.GetChild(0).GetChild(2).GetComponent<TMP_Text>().text;
        searchTerm = searchTerm.Substring(0, searchTerm.Length - 1);
        string[] tableNames = { "", "diary", "achievements", "tasks" };
        string[] searchOptions = {
            " WHERE title like '%"+searchTerm+"%' OR text like '%"+searchTerm+"%'",
            " WHERE title like '%"+searchTerm+"%'",
            " WHERE text like '%"+searchTerm+"%'",
            " WHERE date like '%"+searchTerm+"%'",
        };
        int chosenOpt = searchDropdown.transform.GetComponent<TMP_Dropdown>().value;
        List<string> textToDisplay = new List<string>();

        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open();
        for (int i = 1; i < tableNames.Length; i++)
        {
            if (checkboxPanel.transform.GetChild(i).GetComponent<Toggle>().isOn)
            {
                string query = "SELECT * FROM " + tableNames[i];
                query += searchOptions[chosenOpt];
                //Debug.Log(query);

                IDbCommand dbcmd = dbconn.CreateCommand();
                dbcmd.CommandText = query;
                IDataReader reader = dbcmd.ExecuteReader();
                
                /* Si pe asta am scos-o il deranja id ala.
                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    string text = reader.GetString(1) + " | " + reader.GetString(2) + " | " + reader.GetString(3) + " | " + reader.GetValue(4);
                    textToDisplay.Add(text);

                    Debug.Log("id= " + id + "  text =" + text);
                }
                dbcmd.Dispose();
                dbcmd = null;
                */
            }
        }
        dbconn.Close();
        dbconn = null;

        int pageInc = (page - 1) * q;
        for (int i = 0; i < q; i++)
        {
            entries.transform.GetChild(i).GetChild(0).GetComponent<TMP_InputField>().text = textToDisplay[i + pageInc];
        }
    }

    public void Do_p004_Action_Change_Exercise()
    {
        Debug.Log("We post everything Here");
        //string exercise = refInput.transform.GetComponent<Input_References>()
        //                .new_exercise_exercise.transform.GetComponent<TMP_InputField>().text;

        string query = "SELECT exercise FROM training WHERE place_order =" + 1;
        Debug.Log(query);
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out string text);
        refTrainingOutput.transform.GetComponent<Training_References>().output_field[0].transform.GetComponent<TMP_Text>().text = text;

        //transform.parent.parent.GetChild(1).gameObject.SetActive(true);
        //transform.parent.gameObject.SetActive(false);
    }

    public void doAction_EnterPanel(int obj){
        refObj.transform.GetComponent<References>().main.SetActive(false);
        refObj.transform.GetComponent<References>().panels[obj].SetActive(true);
    }

    public void doAction_ExitPanel(){
        refObj.transform.GetComponent<References>().main.SetActive(true);
        transform.parent.gameObject.SetActive(false);
    }

    public void doAction_ClosePanel()
    {
        transform.parent.gameObject.SetActive(false);
    }

    public void clearAllTicks(){
        for(int i=0; i<checkboxPanel.transform.childCount-1; i++){
            checkboxPanel.transform.GetChild(i).GetComponent<Toggle>().isOn = false;
        }
    }

    public void checkAllTicks(){
        if(checkboxPanel.transform.GetChild(0).GetComponent<Toggle>().isOn == true){
            for(int i=1; i<checkboxPanel.transform.childCount-1; i++){
                checkboxPanel.transform.GetChild(i).GetComponent<Toggle>().isOn = true;
            }
        }
    }

    public int calendar_index;
    public GameObject refCalendar_Script;

    public void doAction_OpenCalendar()
    {
        refObj.transform.GetComponent<References>().panels[21].SetActive(true);
        refCalendar_Script.transform.GetComponent<Calendar>().callendar_index = calendar_index;
    }
}

