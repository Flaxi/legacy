﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.Linq;
public class TextParser : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        inputField = transform.parent.GetChild(1).GetComponent<TMP_InputField>();
        writtenText = transform.parent.GetChild(1).GetChild(0).GetChild(2).GetComponent<TMP_Text>();
    }

    public TMP_InputField inputField;
    public TMP_Text writtenText;
    public int findTag(int tagId, string tag1, string tag2){
        string text = writtenText.text;
        int pos = inputField.caretPosition;
        text = rtfTrim(text);
        pos = rtfCaret(text, pos);
        string substr = text.Substring(0, pos);
        string other = text.Substring(pos, text.Length-pos); //had -1 before
        //Debug.Log(substr+" "+other);

        //you need last occurence here
        var m = Regex.Match(substr, tag1, RegexOptions.RightToLeft);
        int pos1 = m.Success ? m.Index : -1;
        m = Regex.Match(substr, tag2, RegexOptions.RightToLeft);
        int pos2 = m.Success ? m.Index : -1;
        //Debug.Log(pos1+" "+pos2);
        Debug.Log("hmm "+pos1+" "+pos2+" "+substr+" "+other+" pos: "+pos+" "+text.Length+" "+text);
        //Debug.Log(substr);
        //Debug.Log(other);
        if(pos1 == -1 && pos2 == -1){
            m = Regex.Match(other, tag1);
            pos1 = m.Success ? m.Index : -1;
            m = Regex.Match(other, tag2);
            pos2 = m.Success ? m.Index : -1;
            if(pos1 == -1 && pos2 == -1){
                return 0; //nothing found, so you can insert <b>
            }else{
                return 3; //should be <b> on the other side (can't begin with </b>) so put <b></b>
            }
        }else if(pos1 > pos2){ //found <b>
            m = Regex.Match(other, tag1);
            pos1 = m.Success ? m.Index : -1;
            m = Regex.Match(other, tag2);
            pos2 = m.Success ? m.Index : -1;
            //Debug.Log(pos1+" "+pos2+" "+other);
            if(pos1 < pos2 && (pos1!=-1 || pos2 == -1)){ //<b> is first on the other side of the text
                return 1; //insert </b> so: <b> "</b>" <b> - this case should be impossible to find unless pos2=-1
            }else{ //</b> is first on the other side of the text
                if(pos1 == -1 && pos2 == -1) return 1;
                return 2; //insert </b><b> so: <b> "</b><b>" </b>
            }
        }else if(pos1 < pos2){ //found </b>
            m = Regex.Match(other, tag1);
            pos1 = m.Success ? m.Index : -1;
            m = Regex.Match(other, tag2);
            pos2 = m.Success ? m.Index : -1;
            if(pos2 < pos1 && (pos2!=-1 || pos1 == -1)){ //</b> is the first on the other side of the text
                return 0; //insert <b> so: </b> "<b>" </b> - this case should be impossible to find unless pos1=-1
            }else{ //<b> is the first on the other side of the text
                if(pos2 == -1 && pos1 == -1) return 0;
                return 3; //insert <b></b> so: </b>"<b></b>"<b>
            }
        }
        return 0;
    }

    public int boldDetect(string tag1, string tag2){
        //make a function with a switch to get tags from tagId
        //tag1 = "<b>"; 
        //tag2 = "</b>";

        int ok = findTag(0, tag1, tag2); //bold
        //Debug.Log(ok);
        return ok;
    }

    public string rtfTrim(string text){
        //text = text.Trim('\0');
        //text.Replace("\u200B", "");
        string newText = "";
        for (int i = 0; i < text.Length; i++){
            if (text[i] != 8203){
                newText += text[i];
            }
        }
        return newText;
    }
    public int rtfCaret(string text, int pos){
        int i = 0; int k = 0; bool doK = true;
        for(i=0; i<text.Length; i++){
            if(text[i] == '<') doK = false; 
            if(doK){ 
                k++;
                if(k==pos) break;
            }
            if(text[i] == '>') doK = true;
            //Debug.Log(text[i]);
            //Debug.Log(text[i]+0);
            //Debug.Log(System.Char.GetNumericValue(text[i]));
        }
        pos = Mathf.Min(i+1, text.Length);
        return pos;
    }

    public TextMeshProUGUI textBox;
    public void boldButton(string tags){
        //make a function with a switch to get tags from tagId
        string[] split = tags.Split(',');
        string tag1 = split[0]; 
        string tag2 = split[1];
        //string tag3 = tag1.Replace("<", "\u2329"); tag3 = tag3.Replace(">", "\u232A"); //tag3 = tag1.Replace("^", ""); //&lt &gt
        //string tag4 = tag2.Replace("<", "\u2329"); tag4 = tag4.Replace(">", "\u232A"); //3008, 3009
        //Debug.Log(tag3+" "+tag4);
        //Debug.Log(ColorUtility.ToHtmlStringRGB(transform.parent.GetChild(8).GetComponent<Image>().color));

        //here we will add the bold tag <b> or </b>
        int ok;
        if(tag2 == "</color>"){
            ok = boldDetect("<color", tag2);
        }else{
            ok = boldDetect(tag1, tag2);
        }
        string text = writtenText.text;
        int pos = inputField.caretPosition;
        int posOriginal = pos;
        text = rtfTrim(text);
        pos = rtfCaret(text, pos);

        string substr = text.Substring(0, pos);
        string other = text.Substring(pos, text.Length-pos); //had -1 at end
        Debug.Log(inputField.caretPosition+" "+pos+" "+text+" "+text.Length);

        if(tag2 == "</color>"){
            tag1 = tag1.Replace("000000", ColorUtility.ToHtmlStringRGB(transform.parent.GetChild(10).GetComponent<Image>().color));
            if(ok==1) ok = 2;
        }

        if(ok == 0){ //insert <b>, cursor after
            text = substr + tag1 + other;
            inputField.text = text;
            //inputField.caretPosition = posOriginal+tag1.Length; //these are actually for RTF off
        }else if(ok == 1){ //insert </b>, cursor after
            text = substr + tag2 + other;
            inputField.text = text;
            //inputField.caretPosition = posOriginal+tag2.Length;
        }else if(ok == 2){ //insert </b><b>, cursor inside
            if(tag2 != "</color>") text = substr + tag2+" "+tag1 + other;
            else text = substr + tag2+tag1 + other;
            inputField.text = text;
            //inputField.caretPosition = posOriginal+tag2.Length;
        }else if(ok == 3){ //insert <b></b>, cursor inside
            if(tag2 != "</color>") text = substr + tag1+" "+tag2 + other;
            else text = substr + tag1+tag2 + other;
            inputField.text = text;
            //inputField.caretPosition = posOriginal+tag1.Length;
        }
        inputField.caretPosition = posOriginal;
        //Debug.Log(text+"   "+substr+" "+other);
        //Debug.Log(pos+" "+inputField.caretPosition);
        //inputField.ForceLabelUpdate();
        //InputField field = inputField.GetComponent<InputField>();
        inputField.ActivateInputField();
    }
    private string textToDisplay;

    public int lastCursorPos = -1;
    public int lastLength = -1;
    //Update is called once per frame
    void Update(){
        bool ok1 = lastLength == writtenText.text.Length && lastCursorPos == inputField.caretPosition;
        bool ok2 = lastLength + 1 == writtenText.text.Length && lastCursorPos + 1 == inputField.caretPosition;
        bool ok3 = lastLength - 1 == writtenText.text.Length && lastCursorPos - 1 == inputField.caretPosition;
        if(!ok1 && !ok2 && !ok3){
            //boldDetect();
            //if this is necessary make a tagDetect() that cycles through all tags
        }
        lastCursorPos = inputField.caretPosition;
        lastLength = writtenText.text.Length;
    }
}
