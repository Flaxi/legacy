﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform clockContainer;
    public Transform hoursInput;
    public Transform minutesInput;
    public Transform secondsInput;
    public Transform AMPMInput;
    public Transform formatInput;

    public Button HoursUp_Button;
    public Button HoursDown_Button;
    public Button MinutesUp_Button;
    public Button MinutesDown_Button;
    public Button SecondsUp_Button;
    public Button SecondsDown_Button;
    public Button AMPM_Button;
    public Button Format_Button;
    private bool hoursFormat = false; //false = 12, true = 24
    private bool isPM = false; //false = AM, true = PM
    void Start()
    {
        clockContainer = transform.GetChild(1);
        hoursInput = clockContainer.Find("Hours_Input");
        minutesInput = clockContainer.Find("Minutes_Input");
        secondsInput = clockContainer.Find("Seconds_Input");
        AMPMInput = transform.Find("AM_PM");
        formatInput = transform.Find("12_24");

        HoursUp_Button.onClick.AddListener(HoursChangeUp);
        HoursDown_Button.onClick.AddListener(HoursChangeDown);
        MinutesUp_Button.onClick.AddListener(MinutesChangeUp);
        MinutesDown_Button.onClick.AddListener(MinutesChangeDown);
        SecondsUp_Button.onClick.AddListener(SecondsChangeUp);
        SecondsDown_Button.onClick.AddListener(SecondsChangeDown);

        AMPM_Button.onClick.AddListener(AMPMChange);
        Format_Button.onClick.AddListener(FormatChange);

        ChangeFormat();
        ChangeAmPm();
    }

    public void HoursChangeUp(){
        int hours = int.Parse(hoursInput.GetComponent<InputField>().text);
        hours += 1;
        if(hours > 23) hours = 0;
        hours = FormatHours(hours);
        hoursInput.GetComponent<InputField>().text = LeadingZero(hours);
    }

    public void HoursChangeDown(){
        int hours = int.Parse(hoursInput.GetComponent<InputField>().text);
        hours -= 1;
        if(hours < 0) hours = 23;
        hours = FormatHours(hours);
        hoursInput.GetComponent<InputField>().text = LeadingZero(hours);
    }

    public void MinutesChangeUp(){
        int min = int.Parse(minutesInput.GetComponent<InputField>().text);
        min += 1;
        if(min > 59) min = 0;
        minutesInput.GetComponent<InputField>().text = LeadingZero(min);
    }

    public void MinutesChangeDown(){
        int min = int.Parse(minutesInput.GetComponent<InputField>().text);
        min -= 1;
        if(min < 0) min = 59;
        minutesInput.GetComponent<InputField>().text = LeadingZero(min);
    }

    public void SecondsChangeUp(){
        int sec = int.Parse(secondsInput.GetComponent<InputField>().text);
        sec += 1;
        if(sec > 59) sec = 0;
        secondsInput.GetComponent<InputField>().text = LeadingZero(sec);
    }

    public void SecondsChangeDown(){
        int sec = int.Parse(secondsInput.GetComponent<InputField>().text);
        sec -= 1;
        if(sec < 0) sec = 59;
        secondsInput.GetComponent<InputField>().text = LeadingZero(sec);
    }

    public void AMPMChange(){
        isPM = !isPM;
        ChangeAmPm();
    }

    public void FormatChange(){
        hoursFormat = !hoursFormat;
        ChangeFormat();
    }

    private void ChangeAmPm(){
        AMPMInput.GetComponent<Text>().text = isPM ? "PM" : "AM";
    }

    private void ChangeFormat(){
        formatInput.GetComponent<Text>().text = hoursFormat ? "Display: 24H" : "Display: 12H";
        int hours = int.Parse(hoursInput.GetComponent<InputField>().text);
        if(hoursFormat){
            if(isPM == true && hours < 12) hours += 12;
            if(isPM == false && hours == 12) hours = 0; 
        }
        if(!hoursFormat && hours >= 12) isPM = true;
        else isPM = false;
        hours = FormatHours(hours);
        hoursInput.GetComponent<InputField>().text = LeadingZero(hours);

        if(hoursFormat){
            clockContainer.localPosition = new Vector3(420, clockContainer.localPosition.y, clockContainer.localPosition.z);
            AMPMInput.gameObject.SetActive(false);
        }else{
            clockContainer.localPosition = new Vector3(220, clockContainer.localPosition.y, clockContainer.localPosition.z);
            AMPMInput.gameObject.SetActive(true);
        } 
    }

    // Update is called once per frame
    void Update()
    {
        int hours = int.Parse(hoursInput.GetComponent<InputField>().text) % 100;
        if(hours > 24) hours = hours % 10;
        hours = FormatHours(hours);
        hoursInput.GetComponent<InputField>().text = LeadingZero(hours);
    
        int minutes = int.Parse(minutesInput.GetComponent<InputField>().text) % 100;
        if(minutes > 60) minutes = minutes % 10;
        minutesInput.GetComponent<InputField>().text = LeadingZero(minutes);
    
        int seconds = int.Parse(secondsInput.GetComponent<InputField>().text) % 100;
        if(seconds > 60) seconds = seconds % 10;
        secondsInput.GetComponent<InputField>().text = LeadingZero(seconds);
    }

    string LeadingZero(int value){
        if(value < 10){
            return '0'+value.ToString();
        }
        return value.ToString();
    }

    int FormatHours(int value){
        if(!hoursFormat){
            if(value > 12) value = value - 12;
            if(value == 0){ value = 12; isPM = false; }
        }
        return value;
    }
}