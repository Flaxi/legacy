﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TaskClass 
{
    public GameObject name;
    public GameObject deadline;
    public GameObject date;
    public GameObject btnCompleted;
    public GameObject btnFailed;
}

[System.Serializable]
public class TaskPageHandler
{
    public GameObject prev;
    public GameObject next;
    public GameObject number;
}

public class Task_References : MonoBehaviour
{
    public List<TaskClass> tasks = new List<TaskClass>();
    public TaskPageHandler pageHandler;
}
