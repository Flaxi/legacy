﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calendar_References : MonoBehaviour
{
    public GameObject New_Person_Birth_Date;
    public GameObject New_Person_Friends_Since;
    public GameObject Change_Person_Birth_Date;
    public GameObject Change_Person_Friends_Since;
    public GameObject Select_Deadline;
    public GameObject Select_Skill_Started_Learning;
    public GameObject Output_Started_Learning;
    public GameObject[] calendar_array;
    void Start()
    {
        calendar_array = new GameObject[20];
        calendar_array[0] = New_Person_Birth_Date;
        calendar_array[1] = New_Person_Friends_Since;
        calendar_array[2] = Change_Person_Birth_Date;
        calendar_array[3] = Change_Person_Friends_Since;
        calendar_array[4] = Select_Deadline;
        calendar_array[5] = Select_Skill_Started_Learning;
        calendar_array[6] = Output_Started_Learning;
    }
}
