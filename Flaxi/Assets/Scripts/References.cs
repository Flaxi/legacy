﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;
using UnityEngine.UI;
using TMPro;


public class References : MonoBehaviour
{
    public GameObject main;
    public GameObject diary;
    public GameObject diary_list;
    public GameObject settings;
    public GameObject achievements;
    public GameObject tasks;
    public GameObject new_task;
    public GameObject progression;
    public GameObject skills;
    public GameObject new_skill;
    public GameObject change_skill;
    public GameObject jobs;
    public GameObject new_job;
    public GameObject change_job;
    public GameObject people;
    public GameObject new_person;
    public GameObject change_person;
    public GameObject training;
    public GameObject new_exercise;
    public GameObject change_exercise;
    public GameObject search;
    public GameObject calendar;
    public GameObject month_year;
    public GameObject completed_tasks;
    public GameObject failed_tasks;

    // Start is called before the first frame update
    public GameObject[] panels;
    void Start()
    {
        dbInitConnect();

        panels = new GameObject[25];
        panels[0] = main;
        panels[1] = diary;
        panels[2] = diary_list;
        panels[3] = settings;
        panels[4] = achievements;
        panels[5] = tasks;
        panels[6] = new_task;
        panels[7] = progression;
        panels[8] = skills;
        panels[9] = new_skill;
        panels[10] = change_skill;
        panels[11] = jobs;
        panels[12] = new_job;
        panels[13] = change_job;
        panels[14] = people;
        panels[15] = new_person;
        panels[16] = change_person;
        panels[17] = training;
        panels[18] = new_exercise;
        panels[19] = change_exercise;
        panels[20] = search;
        panels[21] = calendar;
        panels[22] = month_year;
        panels[22] = completed_tasks;
        panels[23] = failed_tasks;

    }

    // Update is called once per frame
    private float timepassed = 0.0f;
    public int counter = -2;
    public string Message = "";
    private int steps = 5;
    private float timeStep = 0.2f;
    private int holdForSteps = 5;

    public GameObject event_message_canvas;

    
    void Update()
    {
        if (counter == -1)
        {
            this.event_message_canvas.SetActive(true);
            this.event_message_canvas.transform.GetChild(1).transform.GetComponent<TMP_Text>().text = Message;
            this.event_message_canvas.transform.GetComponent<CanvasGroup>().alpha = 0;
            counter = 0;
            timepassed = Time.realtimeSinceStartup;
        }
        if (counter >= 0)
        {
            if (Time.realtimeSinceStartup > timepassed + timeStep * counter)
            {
                counter++;
                if (counter == steps + holdForSteps) timepassed = Time.realtimeSinceStartup;
                if (counter >= 2 * steps + holdForSteps)
                {
                    counter = -2;
                    this.event_message_canvas.SetActive(false);
                }
            }
            if (counter <= steps)
            {
                this.event_message_canvas.transform.GetComponent<CanvasGroup>().alpha = Time.realtimeSinceStartup - timepassed;
            }
            if (counter >= steps + holdForSteps)
            {
                this.event_message_canvas.transform.GetComponent<CanvasGroup>().alpha = 1 - (Time.realtimeSinceStartup - timepassed);
            }
            //Debug.Log("alpha: "+counter+" "+refSkills.transform.GetComponent<Skill_References>().event_message_canvas.transform.GetComponent<CanvasGroup>().alpha);
        }
    }

    string conn;
    public void dbInitConnect(){
        conn = "URI=file:" + Application.dataPath + "/StreamingAssets/" + "Main.db"; //Path to database.
        IDbConnection dbconn;
        dbconn = (IDbConnection)new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
    }
    public IDataReader executeQuery(string sqlQuery)
    {
        IDbConnection dbconn;
        dbconn = (IDbConnection) new SqliteConnection(conn);
        dbconn.Open(); //Open connection to the database.
        IDbCommand dbcmd = dbconn.CreateCommand();
        dbcmd.CommandText = sqlQuery;
        IDataReader reader = dbcmd.ExecuteReader();

        dbcmd.Dispose();
        dbcmd = null;
        dbconn.Close();
        dbconn = null;
        return reader;
    }

    public void executeNonQuery(string sqlQuery)
    {
        IDataReader reader = executeQuery(sqlQuery);
        reader.Close();
        reader = null;
    }

    public void GetSingleElemFromDB<T>(string sqlQuery, out T elem)
    {
        IDataReader reader = executeQuery(sqlQuery);
        reader.Read();
        if(typeof(T) == typeof(DateTime))
            elem = (T)(object)reader.GetDateTime(0);
        else if(typeof(T) == typeof(int)){
            elem = (T)(object)reader.GetInt32(0);
        }else if(typeof(T) == typeof(string)){
            elem = (T)(object)reader.GetString(0);
        }else{
            elem = (T)(object)null;
            //elem has to be assigned before leaving this method
            //so this case is just to avoid an error, you shouldn't actually reach this case
        }
        reader.Close(); reader = null;
    }
}
