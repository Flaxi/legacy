﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person_References : MonoBehaviour
{
    public GameObject previous_page;
    public GameObject next_page;
    public GameObject page_number;

    public GameObject change_first_name;
    public GameObject change_last_name;
    public GameObject change_status;
    public GameObject change_status_name;
    public GameObject birth_year;
    public GameObject birth_month;
    public GameObject birth_day;
    public GameObject fr_since_year;
    public GameObject fr_since_month;
    public GameObject fr_since_day;
    public GameObject change_current_job;
    public GameObject change_bookmarked;
    public GameObject change_favourite_colour;
    public GameObject person_is_active;

    public GameObject previous_person;
    public GameObject next_person;
    public GameObject current_person;

    // Here come the arrays
    public GameObject[] output_person_name;
    public GameObject[] person_bookmarked_toggle;
    public GameObject[] output_job;
    public GameObject[] output_favourite_colour;
    public GameObject[] output_page_handler;
    public GameObject[] change_person_elements;
    public GameObject[] change_person_page_elements;
    void Start()
    {
        output_page_handler = new GameObject[3];
        output_page_handler[0] = previous_page;
        output_page_handler[1] = next_page;
        output_page_handler[2] = page_number;

        change_person_elements = new GameObject[14];
        change_person_elements[0] = change_first_name;
        change_person_elements[1] = change_last_name;
        change_person_elements[2] = change_status;
        change_person_elements[3] = change_status_name;
        change_person_elements[4] = birth_year;
        change_person_elements[5] = birth_month;
        change_person_elements[6] = birth_day;
        change_person_elements[7] = fr_since_year;
        change_person_elements[8] = fr_since_month;
        change_person_elements[9] = fr_since_day;
        change_person_elements[10] = change_current_job;
        change_person_elements[11] = change_bookmarked;
        change_person_elements[12] = change_favourite_colour;
        change_person_elements[13] = person_is_active ;

        change_person_page_elements = new GameObject[3];
        change_person_page_elements[0] = previous_person;
        change_person_page_elements[1] = next_person;
        change_person_page_elements[2] = current_person;
    }
}
