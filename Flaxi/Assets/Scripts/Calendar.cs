﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;

public class Calendar : MonoBehaviour
{
    public Button Left_Button;
    public Button Right_Button;
    public Button Get_Year_Month_Button;
    public Text MonthAndYear;
    public DateTime currDate = DateTime.Now;
    public GameObject refCalendar;

    public class Day
    {
        public int dayNum;
        public Color dayColor;
        public GameObject obj;
        
        /// Day Constructor
        public Day(int dayNum, Color dayColor, GameObject obj)
        {
            this.dayNum = dayNum;
            this.obj = obj;
            UpdateColor(dayColor);
            UpdateDay(dayNum);
        }

        /// Call this when updating the color so that both the dayColor is updated, as well as the visual color on the screen
        public void UpdateColor(Color newColor)
        {
            obj.GetComponent<RawImage>().color = newColor;
            dayColor = newColor;
        }

        /// When updating the day we decide whether we should show the dayNum based on the color of the day
        /// This means the color should always be updated before the day is updated

        public void UpdateDay(int newDayNum)
        {
            this.dayNum = newDayNum;
            if(dayColor == Color.white || dayColor == Color.green)
            {
                obj.GetComponentInChildren<Text>().text = (dayNum + 1).ToString();
                obj.transform.GetComponent<Button>().interactable = true;
            }
            else
            {
                obj.GetComponentInChildren<Text>().text = "";
                obj.transform.GetComponent<Button>().interactable = false;
            }
        }
    }

    public List<Day> days = new List<Day>();
    public Transform[] weeks;
    private void Start()
    {
        UpdateCalendar(DateTime.Now.Year, DateTime.Now.Month);
        
        Left_Button.onClick.AddListener(LeftMonth);
        Right_Button.onClick.AddListener(RightMonth);
        Get_Year_Month_Button.onClick.AddListener(doAction_Get_Year_Month_Picker);
    }

    void UpdateCalendar(int year, int month)
    {
        DateTime temp = new DateTime(year, month, 1);
        currDate = temp;
        MonthAndYear.text = temp.ToString("MMMM") + " " + temp.Year.ToString();
        int startDay = GetMonthStartDay(year,month);
        int endDay = GetTotalNumberOfDays(year, month);

        ///Create the days
        ///This only happens for our first Update Calendar when we have no Day objects therefore we must create them
        if (days.Count == 0)
        {
            for (int w = 0; w < 6; w++)
            {
                for (int i = 0; i < 7; i++)
                {
                    Day newDay;
                    int currDay = (w * 7) + i;
                    if (currDay < startDay || currDay - startDay >= endDay)
                    {
                        newDay = new Day(currDay - startDay, Color.grey, weeks[w].GetChild(i).gameObject);
                    }
                    else
                    {
                        newDay = new Day(currDay - startDay, Color.white, weeks[w].GetChild(i).gameObject);
                    }
                    days.Add(newDay);
                }
            }
        }
        ///loop through days
        ///Since we already have the days objects, we can just update them rather than creating new ones
        else
        {
            for(int i = 0; i < 42; i++)
            {
                if(i < startDay || i - startDay >= endDay)
                {
                    days[i].UpdateColor(Color.grey);
                }
                else
                {
                    days[i].UpdateColor(Color.white);
                }

                days[i].UpdateDay(i - startDay);
            }
        }

     
            for (int i = 0; i < 42; i++)
            {
                if (i >= startDay && i - startDay <= endDay)
                {
                    // Debug.Log(days[i].dayNum);

                    
                    
                    string giveday = (days[i].dayNum + 1).ToString();
                    string givemonth = currDate.Month.ToString();
                    string giveyear = currDate.Year.ToString();
                    days[i].obj.transform.GetComponent<Button>().onClick.RemoveAllListeners();
                    days[i].obj.transform.GetComponent<Button>().onClick.AddListener(() => Output_Function(giveday, givemonth, giveyear));

                    
                    
                    days[i].obj.transform.GetComponent<Button>().onClick.AddListener(doAction_CloseCalendar);
                }
            }
        
        ///This just checks if today is on our calendar. If so, we highlight it in green
        if (DateTime.Now.Year == year && DateTime.Now.Month == month)
        {
            days[(DateTime.Now.Day - 1) + startDay].UpdateColor(Color.green);
        }

    }

    /// This returns which day of the week the month is starting on
    int GetMonthStartDay(int year, int month)
    {
        DateTime temp = new DateTime(year, month, 1);

        //DayOfWeek Sunday == 0, Saturday == 6 etc.
        return (int)temp.DayOfWeek;
    }

    /// Gets the number of days in the given month.
    int GetTotalNumberOfDays(int year, int month)
    {
        return DateTime.DaysInMonth(year, month);
    }

    /// This either adds or subtracts one month from our currDate.
    /// The arrows will use this function to switch to past or future months
    public void LeftMonth()
    {
        currDate = currDate.AddMonths(-1);
        UpdateCalendar(currDate.Year, currDate.Month);
    }

    public void RightMonth()
    {
        currDate = currDate.AddMonths(1);
        UpdateCalendar(currDate.Year, currDate.Month);
    }

    public int callendar_index;

    public void Output_Function(string day, string month, string year)
    {
        string returnstring;
        int intday, intmonth;

        Int32.TryParse(day, out intday);
        Int32.TryParse(month, out intmonth);
        Debug.Log(intday);
        Debug.Log(intmonth);

        if (intday < 9)
            day = 0 + day;

        if (intmonth < 9)
            month = 0 + month;
        //if ( % 10 < 10) 
        //  Debug.Log("Day is: ");
        //  Debug.Log(day);

        //  Debug.Log(" Month is: ");
        //    Debug.Log(month);

        // Debug.Log(" Year is: ");
        //  Debug.Log(year);
        returnstring = year + "-" + month + "-" + day;
        refCalendar.transform.GetComponent<Calendar_References>().calendar_array[callendar_index].transform.GetComponent<TMP_Text>().text = returnstring;
    }

    public GameObject refObj;
    public void doAction_CloseCalendar()
    {
        refObj.transform.GetComponent<References>().panels[21].SetActive(false);
    }

    public GameObject Year_Picked;
    public GameObject Month_Picked;
    public void doAction_Get_Year_Month_Picker()
    {
        string year_text;
        year_text = Year_Picked.transform.GetComponent<Text>().text;
        Debug.Log(year_text);

        string month_text;
        month_text = Month_Picked.transform.GetComponent<Text>().text;
        //  currDate.Year = Convert.ToDateTime(year_text);

        DateTime year_month_date = new DateTime(Convert.ToInt32(year_text), Convert.ToInt32(month_text), 1);
        currDate = year_month_date;

        UpdateCalendar(currDate.Year, currDate.Month);
    }
}
