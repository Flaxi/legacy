﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;
using UnityEngine.UI;
using TMPro;

public class Page_Handler : MonoBehaviour
{
    public GameObject refTrainingOutput;
    public GameObject refPerson;
    public GameObject refTasks;
    public GameObject refSkills;
    public GameObject refCalendar;

    public int btn_number;
    //public int btn_task_number;
    public bool is_admin_camera = false;

    void start(){
        refObj.transform.GetComponent<References>().dbInitConnect();
    }
    
    public void Do_001_Action_Open_Exercises()
    {
        int table_number;
        int i = 1;
        int current_page = PlayerPrefs.GetInt("Current_Page", 0);

        refTrainingOutput.transform.GetComponent<Training_References>().switch_page[0].transform.GetComponent<Button>().interactable = true;
        refTrainingOutput.transform.GetComponent<Training_References>().switch_page[1].transform.GetComponent<Button>().interactable = true;

        string query = "SELECT COUNT(*) FROM training";
        IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);
        reader.Read();
        table_number = reader.GetInt32(0); // aici aflam cate elemente are tabelul
        reader.Close(); reader = null;

        if (table_number <= 20)
        {
            refTrainingOutput.transform.GetComponent<Training_References>().switch_page[0].transform.GetComponent<Button>().interactable = false;
            refTrainingOutput.transform.GetComponent<Training_References>().switch_page[1].transform.GetComponent<Button>().interactable = false;
            refTrainingOutput.transform.GetComponent<Training_References>().switch_page[2].transform.GetComponent<TMP_Text>().text = "1/1";
        }

        if (current_page == 0 && table_number > 20)
            refTrainingOutput.transform.GetComponent<Training_References>().switch_page[0].transform.GetComponent<Button>().interactable = false;

        if ((current_page + 1) * 20 > table_number && table_number > 20)
            refTrainingOutput.transform.GetComponent<Training_References>().switch_page[1].transform.GetComponent<Button>().interactable = false;

        string output_page = (current_page + 1) + " / " + ((table_number / 20) + 1);
        refTrainingOutput.transform.GetComponent<Training_References>().switch_page[2].transform.GetComponent<TMP_Text>().text = output_page;

        while (i + current_page * 20 <= table_number && i <= 20)
        {
            // La deschidere restartam totul
            query = "SELECT last_date FROM training WHERE place_order =" + (i + current_page * 20);
            refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out DateTime date_test);

            if (date_test != DateTime.Today)
            {
                query = "UPDATE training SET calories_burned_today = 0 WHERE place_order =" + (i + current_page * 20);
                refObj.transform.GetComponent<References>().executeNonQuery(query);

                query = "UPDATE training SET last_date = CURRENT_DATE WHERE place_order =" + (i + current_page * 20);
                refObj.transform.GetComponent<References>().executeNonQuery(query);
            }

            //////////////////////////F I R S T ////// E L E M E N T//////////////////////////////
            query = "SELECT exercise FROM training WHERE place_order =" + (i + current_page * 20);
            refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out string text);
            refTrainingOutput.transform.GetComponent<Training_References>().output_field[i - 1].transform.GetComponent<TMP_Text>().text = text;

            ////////////////////////////////////S E C O N D////////////////////////////////////////////
            query = "SELECT calories_burned_exercise FROM training WHERE place_order =" + (i + current_page * 20);
            refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int field);
            refTrainingOutput.transform.GetComponent<Training_References>().output_field1[i - 1].transform.GetComponent<TMP_Text>().text = field.ToString();

            ////////////////////////////////////T H I R D////////////////////////////////////////////
            query = "SELECT calories_burned_today FROM training WHERE place_order =" + (i + current_page * 20);
            refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out field);
            refTrainingOutput.transform.GetComponent<Training_References>().output_field2[i - 1].transform.GetComponent<TMP_Text>().text = field.ToString();

            refTrainingOutput.transform.GetComponent<Training_References>().button_click[i - 1].transform.GetComponent<Button>().interactable = true;
            i++;
        }

        while (i <= 20)
        {
            refTrainingOutput.transform.GetComponent<Training_References>().output_field[i - 1].transform.GetComponent<TMP_Text>().text = " None ";
            refTrainingOutput.transform.GetComponent<Training_References>().output_field1[i - 1].transform.GetComponent<TMP_Text>().text = " - ";
            refTrainingOutput.transform.GetComponent<Training_References>().output_field2[i - 1].transform.GetComponent<TMP_Text>().text = " - ";
            refTrainingOutput.transform.GetComponent<Training_References>().button_click[i - 1].transform.GetComponent<Button>().interactable = false;
            i++;
        }
    }

    public void Do_001_Action_Button_Click()
    {
        int current_page;
        current_page = PlayerPrefs.GetInt("Current_Page", 0);
        int i = btn_number;

        string query = "SELECT last_date FROM training WHERE place_order =" + (i + 1 + current_page * 20);
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out DateTime date_test);

        if (date_test == DateTime.Today)
        {
            string add_calories = refTrainingOutput.transform.GetComponent<Training_References>().output_field1[i].transform.GetComponent<TMP_Text>().text;
            int calories_number = int.Parse(add_calories);

            query = "UPDATE training SET calories_burned_today = calories_burned_today +" + calories_number + " WHERE place_order =" + (i + 1 + current_page * 20);
            refObj.transform.GetComponent<References>().executeNonQuery(query);
            Do_001_Action_Open_Exercises();
        }
        else
        {
            query = "UPDATE training SET calories_burned_today = 0 WHERE place_order =" + (i + current_page * 20 + 1);
            refObj.transform.GetComponent<References>().executeNonQuery(query);
            query = "UPDATE training SET last_date = CURRENT_DATE WHERE place_order =" + (i + 1 + current_page * 20);
            refObj.transform.GetComponent<References>().executeNonQuery(query);
        }
        //refTrainingOutput.transform.GetComponent<Training_References>().output_field[i - 1].transform.GetComponent<TMP_Text>().text = text;
    }

    public void Do_001_Previous_Page()
    {
        PlayerPrefs.SetInt("Current_Page", PlayerPrefs.GetInt("Current_Page") - 1);
        Debug.Log(PlayerPrefs.GetInt("Current_Page"));
        Do_001_Action_Open_Exercises();
    }

    public void Do_001_Next_Page()
    {
        PlayerPrefs.SetInt("Current_Page", PlayerPrefs.GetInt("Current_Page") + 1);
        Debug.Log(PlayerPrefs.GetInt("Current_Page"));
        Do_001_Action_Open_Exercises();
    }

    public void Do_001_Output_Change_Exercises()
    {
        int current_exercise = PlayerPrefs.GetInt("Current_Exercise", 1);

        refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[5].transform.GetComponent<Button>().interactable = true;
        refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[6].transform.GetComponent<Button>().interactable = true;

        string query = "SELECT COUNT(*)  FROM training;";
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int number_of_exercises);

        if (current_exercise == 1)
            refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[5].transform.GetComponent<Button>().interactable = false;

        if (current_exercise == number_of_exercises)
            refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[6].transform.GetComponent<Button>().interactable = false;

        string output_current_exercise = current_exercise + " / " + number_of_exercises;
        refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[4].transform.GetComponent<TMP_Text>().text = output_current_exercise;

        //////////////////////////F I R S T ////// E L E M E N T//////////////////////////////
        query = "SELECT exercise FROM training WHERE place_order =" + current_exercise;
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out string text);
        refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[0].transform.GetComponent<TMP_InputField>().text = text;

        ////////////////////////////////////S E C O N D////////////////////////////////////////////
        query = "SELECT calories_burned_exercise FROM training WHERE place_order =" + current_exercise;
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int field);
        refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[1].transform.GetComponent<TMP_InputField>().text = field.ToString();

        ////////////////////////////////////T H I R D////////////////////////////////////////////
        query = "SELECT enjoyment_level FROM training WHERE place_order =" + current_exercise;
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out field);
        refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[2].transform.GetComponent<TMP_InputField>().text = field.ToString();

        query = "SELECT difficulty FROM training WHERE place_order =" + current_exercise;
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out text);
        refTrainingOutput.transform.GetComponent<Training_References>().change_exercise_output[3].transform.GetChild(0).GetComponent<TMP_Text>().text = text;
    }

    public void Do_001_Previous_Exercise()
    {
        PlayerPrefs.SetInt("Current_Exercise", PlayerPrefs.GetInt("Current_Exercise") - 1);
        Debug.Log(PlayerPrefs.GetInt("Current_Exercise"));
        Do_001_Output_Change_Exercises();
    }

    public void Do_001_Next_Exercise()
    {
        PlayerPrefs.SetInt("Current_Exercise", PlayerPrefs.GetInt("Current_Exercise") + 1);
        Debug.Log(PlayerPrefs.GetInt("Current_Exercise"));
        Do_001_Output_Change_Exercises();
    }

    public void Do_001_Action_Change_Exercise()
    {
        string exercise = refTrainingOutput.transform.GetComponent<Training_References>()
                        .change_exercise_output[0].transform.GetComponent<TMP_InputField>().text;
        string calories_burned_exercise = refTrainingOutput.transform.GetComponent<Training_References>()
                                        .change_exercise_output[1].transform.GetComponent<TMP_InputField>().text;
        string enjoyment_level = refTrainingOutput.transform.GetComponent<Training_References>()
                               .change_exercise_output[2].transform.GetComponent<TMP_InputField>().text;

        string[] difficulty_list = new string[] { "None", "Low", "Medium", "High", "Extreme" };
        int difficulty_int = refTrainingOutput.transform.GetComponent<Training_References>()
                            .change_exercise_output[3].transform.GetComponent<TMP_Dropdown>().value;
        string difficulty = difficulty_list[difficulty_int];

        string query = "UPDATE training " +
                       "SET exercise = '" + exercise + "', " +
                       "calories_burned_exercise = " + calories_burned_exercise + ", "
                       + "enjoyment_level =" + enjoyment_level + ", "
                       + "difficulty = '" + difficulty + "' WHERE place_order = " + PlayerPrefs.GetInt("Current_Exercise") + ";";
        //Debug.Log(query);
        refObj.transform.GetComponent<References>().executeNonQuery(query);
    }
    /// //////////////////////////////////////////////////////////////////// /////////////////////////////////////////////////////////////////

    public void Do_002_Action_Open_Persons()
    {
        int current_page = PlayerPrefs.GetInt("Persons_Current_Page", 0);
        refPerson.transform.GetComponent<Person_References>().output_page_handler[0].transform.GetComponent<Button>().interactable = true;
        refPerson.transform.GetComponent<Person_References>().output_page_handler[1].transform.GetComponent<Button>().interactable = true;

        string query = "SELECT count(id) FROM family WHERE is_active = 1;";
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int total_persons); // aici aflam cate elemente are tabelul

        if (total_persons <= 6)
        {
            refPerson.transform.GetComponent<Person_References>().output_page_handler[0].transform.GetComponent<Button>().interactable = false;
            refPerson.transform.GetComponent<Person_References>().output_page_handler[1].transform.GetComponent<Button>().interactable = false;
            refPerson.transform.GetComponent<Person_References>().output_page_handler[2].transform.GetComponent<TMP_Text>().text = "1/1";
        }

        if (current_page == 0 && total_persons > 6)
            refPerson.transform.GetComponent<Person_References>().output_page_handler[0].transform.GetComponent<Button>().interactable = false;

        if ((current_page + 1) * 6 > total_persons && total_persons > 6)
            refPerson.transform.GetComponent<Person_References>().output_page_handler[1].transform.GetComponent<Button>().interactable = false;

        string output_page = (current_page + 1) + " / " + ((total_persons / 6) + 1);
        refPerson.transform.GetComponent<Person_References>().output_page_handler[2].transform.GetComponent<TMP_Text>().text = output_page;

        // HERE WE START PUTTING EVERYTHING TOGETHER
        string[] first_name = new string[6];
        string[] last_name = new string[6];
        bool[] is_bookmarked = new bool[6];
        string[] job_name = new string[6];
        string[] colour_name = new string[6];
        query = "SELECT first_name, last_name, bookmarked, job_name, favourite_colour FROM family WHERE is_active = 1;";
        IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);
        
        // Aici ne asiguram ca incepe de la elementul dorit array ul
        if (current_page != 0)
            for (int j = 1; j <= current_page * 6; j++)
                reader.Read();

        int i = 1;
        while (i + current_page * 6 <= total_persons && i <= 6)
        {
            reader.Read();
            first_name[i - 1] = reader.GetString(0);
            last_name[i - 1] = reader.GetString(1);
            is_bookmarked[i - 1] = reader.GetBoolean(2);
            job_name[i - 1] = reader.GetString(3);
            colour_name[i - 1] = reader.GetString(4);

            refPerson.transform.GetComponent<Person_References>().output_person_name[i - 1]
                .transform.GetComponent<TMP_Text>().text = first_name[i - 1] + " " + last_name[i - 1];
            refPerson.transform.GetComponent<Person_References>().person_bookmarked_toggle[i - 1]
                .transform.GetComponent<Toggle>().isOn = is_bookmarked[i - 1];
            refPerson.transform.GetComponent<Person_References>().output_job[i - 1]
                .transform.GetComponent<TMP_Text>().text = job_name[i - 1];
            refPerson.transform.GetComponent<Person_References>().output_favourite_colour[i - 1]
                .transform.GetComponent<TMP_Text>().text = colour_name[i - 1];
            i++;
        }
        reader.Close(); reader = null;

        while (i <= 6)
        {
            refPerson.transform.GetComponent<Person_References>().output_person_name[i - 1]
                .transform.GetComponent<TMP_Text>().text = " - ";
            refPerson.transform.GetComponent<Person_References>().person_bookmarked_toggle[i - 1]
                .transform.GetComponent<Toggle>().isOn = false;
            refPerson.transform.GetComponent<Person_References>().output_job[i - 1]
                .transform.GetComponent<TMP_Text>().text = " - ";
            refPerson.transform.GetComponent<Person_References>().output_favourite_colour[i - 1]
               .transform.GetComponent<TMP_Text>().text = " - ";
            i++;
        }
    }

    public void Do_002_Previous_Page()
    {
        PlayerPrefs.SetInt("Persons_Current_Page", PlayerPrefs.GetInt("Persons_Current_Page") - 1);
        Debug.Log(PlayerPrefs.GetInt("Persons_Current_Page"));
        Do_002_Action_Open_Persons();
    }

    public void Do_002_Next_Page()
    {
        PlayerPrefs.SetInt("Persons_Current_Page", PlayerPrefs.GetInt("Persons_Current_Page") + 1);
        Debug.Log(PlayerPrefs.GetInt("Persons_Current_Page"));
        Do_002_Action_Open_Persons();
    }

    public void Do_002_Output_Change_Person()
    {
        int current_person = PlayerPrefs.GetInt("Person_Current", 1);
        refPerson.transform.GetComponent<Person_References>().change_person_page_elements[0].transform.GetComponent<Button>().interactable = true;
        refPerson.transform.GetComponent<Person_References>().change_person_page_elements[1].transform.GetComponent<Button>().interactable = true;

        string query = "SELECT COUNT(*)  FROM family;";
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int number_of_people); // aici aflam cate elemente are tabelul

        if (current_person == 1)
            refPerson.transform.GetComponent<Person_References>().change_person_page_elements[0].transform.GetComponent<Button>().interactable = false;

        if (current_person == number_of_people)
            refPerson.transform.GetComponent<Person_References>().change_person_page_elements[1].transform.GetComponent<Button>().interactable = false;

        string output_current_person = current_person + " / " + number_of_people;
        refPerson.transform.GetComponent<Person_References>().change_person_page_elements[2].transform.GetComponent<TMP_Text>().text = output_current_person;

        query = "SELECT first_name, last_name, person_status, status_name, birth_date, friends_since, bookmarked, favourite_colour, job_name, is_active FROM family WHERE id =" + current_person;
        IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);
        reader.Read();

        string first_name = reader.GetString(0);
        string last_name = reader.GetString(1);
        string person_status = reader.GetString(2);
        string status_name = reader.GetString(3);
        string birth_date = reader.GetString(4);
        string friends_since = reader.GetString(5);
        bool is_bookmarked = reader.GetBoolean(6);
        string favourite_colour = reader.GetString(7);
        string job_name = reader.GetString(8);
        bool is_active = reader.GetBoolean(9);
        reader.Close(); reader = null;

        refPerson.transform.GetComponent<Person_References>().change_person_elements[0]
            .transform.GetComponent<TMP_InputField>().text = first_name;
        refPerson.transform.GetComponent<Person_References>().change_person_elements[1]
            .transform.GetComponent<TMP_InputField>().text = last_name;
        refPerson.transform.GetComponent<Person_References>().change_person_elements[2]
            .transform.GetChild(0).transform.GetComponent<TMP_Text>().text = person_status;
        refPerson.transform.GetComponent<Person_References>().change_person_elements[3]
            .transform.GetComponent<TMP_InputField>().text = status_name;
        //Birth Day
        refCalendar.transform.GetComponent<Calendar_References>().calendar_array[2]
            .transform.GetComponent<TMP_Text>().text = birth_date;
        //Friends since
        refCalendar.transform.GetComponent<Calendar_References>().calendar_array[3]
            .transform.GetComponent<TMP_Text>().text = friends_since;
        refPerson.transform.GetComponent<Person_References>().change_person_elements[11]
            .transform.GetComponent<Toggle>().isOn = is_bookmarked;
        refPerson.transform.GetComponent<Person_References>().change_person_elements[12]
            .transform.GetComponent<TMP_InputField>().text = favourite_colour;
        refPerson.transform.GetComponent<Person_References>().change_person_elements[10]
            .transform.GetComponent<TMP_InputField>().text = job_name;
        refPerson.transform.GetComponent<Person_References>().change_person_elements[13]
            .transform.GetComponent<Toggle>().isOn = is_active;
    }

    public void Do_002_Previous_Person()
    {
        PlayerPrefs.SetInt("Person_Current", PlayerPrefs.GetInt("Person_Current") - 1);
        Do_002_Output_Change_Person();
    }

    public void Do_002_Next_Person()
    {
        PlayerPrefs.SetInt("Person_Current", PlayerPrefs.GetInt("Person_Current") + 1);
        Do_002_Output_Change_Person();
    }

    public void Do_002_Action_Change_Person()
    {
        string first_name = refPerson.transform.GetComponent<Person_References>().change_person_elements[0]
                            .transform.GetComponent<TMP_InputField>().text;
        string last_name = refPerson.transform.GetComponent<Person_References>().change_person_elements[1]
                           .transform.GetComponent<TMP_InputField>().text;
        string person_status = refPerson.transform.GetComponent<Person_References>().change_person_elements[2]
                               .transform.GetChild(0).transform.GetComponent<TMP_Text>().text;
        string status_name = refPerson.transform.GetComponent<Person_References>().change_person_elements[3]
                             .transform.GetComponent<TMP_InputField>().text;
        //Birth Day
        string birth_day = refCalendar.transform.GetComponent<Calendar_References>()
                           .calendar_array[2].transform.GetComponent<TMP_Text>().text;
        //Friends since
        string friends_since = refCalendar.transform.GetComponent<Calendar_References>()
                               .calendar_array[3].transform.GetComponent<TMP_Text>().text;
        bool is_bookmarked = refPerson.transform.GetComponent<Person_References>().change_person_elements[11]
                            .transform.GetComponent<Toggle>().isOn;
        string favourite_colour = refPerson.transform.GetComponent<Person_References>().change_person_elements[12]
                                 .transform.GetComponent<TMP_InputField>().text;
        string job_name = refPerson.transform.GetComponent<Person_References>().change_person_elements[10]
                        .transform.GetComponent<TMP_InputField>().text;
        bool is_active = refPerson.transform.GetComponent<Person_References>().change_person_elements[13]
                        .transform.GetComponent<Toggle>().isOn;
        int arc = Convert.ToInt32(is_bookmarked);

        string query = "UPDATE family " +
                       "SET first_name ='" + first_name + "', last_name = '" + last_name + "', person_status ='" +
                        person_status + "', status_name = '" + status_name + "', birth_date = '" + birth_day + "', friends_since = '" +
                        friends_since + "', bookmarked = '" + Convert.ToInt32(is_bookmarked) + "', favourite_colour = '" + favourite_colour + "', job_name = '" +
                        job_name + "', is_active = '" + Convert.ToInt32(is_active) + "' "
                        + "WHERE id = " + PlayerPrefs.GetInt("Person_Current", 1) + ";";
        //Debug.Log(query);
        refObj.transform.GetComponent<References>().executeNonQuery(query);
    }
    /// //////////////////////////////////////////////////////////////////// /////////////////////////////////////////////////////////////////
    public void Do_003_Action_Open_Tasks()
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////// T H I S    I S     T H E    P A R T     F O R    A D D I N G     L I S T E N E R S//////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int i;
        for(i=0; i<refTasks.transform.GetComponent<Task_References>().tasks.Count; i++)
        {
            refTasks.transform.GetComponent<Task_References>().tasks[i].btnCompleted.transform.GetComponent<Button>()
                .onClick.RemoveAllListeners();
            refTasks.transform.GetComponent<Task_References>().tasks[i].btnCompleted.transform.GetComponent<Button>()
                .onClick.AddListener(() => Do_003_Action_Completed_Click(i));

            refTasks.transform.GetComponent<Task_References>().tasks[i].btnFailed.transform.GetComponent<Button>()
                .onClick.RemoveAllListeners();
            refTasks.transform.GetComponent<Task_References>().tasks[i].btnFailed.transform.GetComponent<Button>()
                .onClick.AddListener(() => Do_003_Action_Failed_Click(i));
        }

        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>()
            .onClick.RemoveAllListeners();
        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>()
            .onClick.AddListener(() => Do_003_Previous_Page());

        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>()
            .onClick.RemoveAllListeners();
        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>()
            .onClick.AddListener(() => Do_003_Next_Page());

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////// T H I S    I S     T H E    P A R T     F O R    A D D I N G     L I S T E N E R S//////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        i = 1;
        int current_page = PlayerPrefs.GetInt("Current_Page_Task", 0);

        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = true;
        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = true;
        string query = "SELECT COUNT(*) FROM tasks WHERE is_in_progress = 1;";
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int table_number); // aici aflam cate elemente are tabelul
        Debug.Log("There are " + table_number + " elements in the table");
        Debug.Log("Current page is: " + PlayerPrefs.GetInt("Current_Page_Task", 0));

        if (current_page < 0)
        {
            current_page = 0;
            PlayerPrefs.SetInt("Current_Page_Task", current_page);
        }
        if ((current_page + 1) * 5 > table_number)
        {
            current_page = table_number / 5;
            PlayerPrefs.SetInt("Current_Page_Task", current_page);
        }

        if (table_number <= 5)
        {
            refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = false;
            refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = false;
            refTasks.transform.GetComponent<Task_References>().pageHandler.number.transform.GetComponent<TMP_Text>().text = "1/1";
        }
        else
        {
            if (current_page == 0 && table_number > 5)
                refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = false;

            if ((current_page + 1) * 5 > table_number && table_number > 5)
                refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = false;

            string output_page = (current_page + 1) + " / " + ((table_number / 5) + 1);
            refTasks.transform.GetComponent<Task_References>().pageHandler.number.transform.GetComponent<TMP_Text>().text = output_page;
        }

        while (i + current_page * 5 <= table_number && i <= 5)
        {
            /////////////////////////// E L E M E N T S   //////////////////////////////
            query = "SELECT title, deadline FROM tasks WHERE is_in_progress = 1 ORDER BY deadline ASC LIMIT 1 OFFSET " + (i - 1 + current_page * 5);
            IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);
            reader.Read();
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].name.transform.GetComponent<TMP_Text>().text = reader.GetString(0);
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].deadline.transform.GetComponent<TMP_Text>().text = reader.GetString(1);
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].btnCompleted.transform.GetComponent<Button>().interactable = true;
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].btnFailed.transform.GetComponent<Button>().interactable = true;
            reader.Close(); reader = null;
            i++;
        }

        while (i <= 5)
        {
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].name.transform.GetComponent<TMP_Text>().text = " - ";
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].deadline.transform.GetComponent<TMP_Text>().text = " - ";
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].btnCompleted.transform.GetComponent<Button>().interactable = false;
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].btnFailed.transform.GetComponent<Button>().interactable = false;
            i++;
        }
    }


    public void Do_003_Action_Completed_Click(int btn_task_number)
    {
        int current_page;
        current_page = PlayerPrefs.GetInt("Current_Page_Task", 0);
        int i = btn_task_number;

        string query = "SELECT id FROM tasks WHERE is_in_progress = 1 ORDER BY deadline ASC LIMIT 1 OFFSET " + (i + current_page * 5);
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int id_number);
        IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);

        query = "UPDATE tasks SET is_in_progress = 0 WHERE id =" + id_number;
        refObj.transform.GetComponent<References>().executeNonQuery(query);

        query = "UPDATE tasks SET completed_date ='" + DateTime.Today.Year.ToString() + "-" + DateTime.Today.Month.ToString() + "-" + DateTime.Today.Day.ToString()
                + "' WHERE id =" + id_number;
        refObj.transform.GetComponent<References>().executeNonQuery(query);

        Do_003_Action_Open_Tasks();
    }

    public void Do_003_Action_Failed_Click(int btn_task_number)
    {
        int current_page;
        current_page = PlayerPrefs.GetInt("Current_Page_Task", 0);
        int i = btn_task_number;

        string query = "SELECT id FROM tasks WHERE is_in_progress = 1 ORDER BY deadline ASC LIMIT 1 OFFSET " + (i + current_page * 5);
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int id_number);

        query = "UPDATE tasks SET is_in_progress = 0 WHERE id =" + id_number;
        refObj.transform.GetComponent<References>().executeNonQuery(query);

        query = "UPDATE tasks SET abandon_date ='" + DateTime.Today.Year.ToString() + "-" + DateTime.Today.Month.ToString() + "-" + DateTime.Today.Day.ToString()
        + "' WHERE id =" + id_number;
        refObj.transform.GetComponent<References>().executeNonQuery(query);

        Do_003_Action_Open_Tasks();
    }


    public void Do_003_Previous_Page()
    {
        PlayerPrefs.SetInt("Current_Page_Task", PlayerPrefs.GetInt("Current_Page_Task") - 1);
        Debug.Log("Current Page Will be: " + PlayerPrefs.GetInt("Current_Page_Task"));
        Do_003_Action_Open_Tasks();
    }

    public void Do_003_Next_Page()
    {
        PlayerPrefs.SetInt("Current_Page_Task", PlayerPrefs.GetInt("Current_Page_Task") + 1);
        Debug.Log("Current Page Will be: " + PlayerPrefs.GetInt("Current_Page_Task"));
        Do_003_Action_Open_Tasks();
    }

    public void Do_003_Action_Open_Completed_Tasks()
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////// T H I S    I S     T H E    P A R T     F O R    A D D I N G     L I S T E N E R S//////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().onClick.RemoveAllListeners();
        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().onClick.AddListener(() => Do_003_Previous_Completed_Page());
        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().onClick.RemoveAllListeners();
        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().onClick.AddListener(() => Do_003_Next_Completed_Page());

        int i = 1;
        int current_page = PlayerPrefs.GetInt("Current_Completed_Page_Task", 0);
        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = true;
        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = true;

        string query = "SELECT COUNT(*) FROM tasks WHERE completed_date IS NOT NULL;";
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int table_number);

        if (current_page < 0)
        {
            current_page = 0;
            PlayerPrefs.SetInt("Current_Completed_Page_Task", current_page);
        }
        if ((current_page + 1) * 5 > table_number)
        {
            current_page = table_number / 5;
            PlayerPrefs.SetInt("Current_Completed_Page_Task", current_page);
        }

        if (table_number <= 5)
        {
            refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = false;
            refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = false;
            refTasks.transform.GetComponent<Task_References>().pageHandler.number.transform.GetComponent<TMP_Text>().text = "1/1";
        }
        else
        {
            if (current_page == 0 && table_number > 5)
                refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = false;

            if ((current_page + 1) * 5 > table_number && table_number > 5)
                refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = false;

            string output_page = (current_page + 1) + " / " + ((table_number / 5) + 1);
            refTasks.transform.GetComponent<Task_References>().pageHandler.number.transform.GetComponent<TMP_Text>().text = output_page;
        }

        while (i + current_page * 5 <= table_number && i <= 5)
        {
            /////////////////////////// E L E M E N T S   //////////////////////////////
            query = "SELECT title, deadline, completed_date FROM tasks WHERE completed_date IS NOT NULL ORDER BY completed_date DESC LIMIT 1 OFFSET " + (i - 1 + current_page * 5);
            IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);
            reader.Read();
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].name.transform.GetComponent<TMP_Text>().text = reader.GetString(0);
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].deadline.transform.GetComponent<TMP_Text>().text = reader.GetString(1);
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].date.transform.GetComponent<TMP_Text>().text = reader.GetString(2);
            reader.Close(); reader = null;
            i++;
        }

        while (i <= 5)
        {
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].name.transform.GetComponent<TMP_Text>().text = " - ";
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].deadline.transform.GetComponent<TMP_Text>().text = " - ";
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].date.transform.GetComponent<TMP_Text>().text = " - ";
            i++;
        }
    }

    public void Do_003_Previous_Completed_Page()
    {
        PlayerPrefs.SetInt("Current_Completed_Page_Task", PlayerPrefs.GetInt("Current_Completed_Page_Task") - 1);
        Debug.Log("Current Completed Page Will be: " + PlayerPrefs.GetInt("Current_Completed_Page_Task"));
        Do_003_Action_Open_Completed_Tasks();
    }

    public void Do_003_Next_Completed_Page()
    {
        PlayerPrefs.SetInt("Current_Completed_Page_Task", PlayerPrefs.GetInt("Current_Completed_Page_Task") + 1);
        Debug.Log("Current Completed Page Will be: " + PlayerPrefs.GetInt("Current_Completed_Page_Task"));
        Do_003_Action_Open_Completed_Tasks();
    }

    public void Do_003_Action_Open_Failed_Tasks()
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////// T H I S    I S     T H E    P A R T     F O R    A D D I N G     L I S T E N E R S//////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().onClick.RemoveAllListeners();
        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>()
            .onClick.AddListener(() => Do_003_Previous_Failed_Page());
        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().onClick.RemoveAllListeners();
        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().onClick.AddListener(() => Do_003_Next_Failed_Page());
        
        int i = 1;
        int current_page = PlayerPrefs.GetInt("Current_Failed_Page_Task", 0);
        
        refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = true;
        refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = true;

        string query = "SELECT COUNT(*) FROM tasks WHERE abandon_date IS NOT NULL;";
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int table_number); // aici aflam cate elemente are tabelul
        Debug.Log("There are " + table_number + " elements in the failed tasks table");
        Debug.Log("Current page is: " + PlayerPrefs.GetInt("Current_Failed_Page_Task", 0));

        if (current_page < 0)
        {
            current_page = 0;
            PlayerPrefs.SetInt("Current_Failed_Page_Task", current_page);
        }
        if ((current_page + 1) * 5 > table_number)
        {
            current_page = table_number / 5;
            PlayerPrefs.SetInt("Current_Failed_Page_Task", current_page);
        }

        if (table_number <= 5)
        {
            refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = false;
            refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = false;
            refTasks.transform.GetComponent<Task_References>().pageHandler.number.transform.GetComponent<TMP_Text>().text = "1/1";
        }
        else
        {
            if (current_page == 0 && table_number > 5)
                refTasks.transform.GetComponent<Task_References>().pageHandler.prev.transform.GetComponent<Button>().interactable = false;

            if ((current_page + 1) * 5 > table_number && table_number > 5)
                refTasks.transform.GetComponent<Task_References>().pageHandler.next.transform.GetComponent<Button>().interactable = false;

            string output_page = (current_page + 1) + " / " + ((table_number / 5) + 1);
            refTasks.transform.GetComponent<Task_References>().pageHandler.number.transform.GetComponent<TMP_Text>().text = output_page;
        }

        while (i + current_page * 5 <= table_number && i <= 5)
        {
            query = "SELECT title, deadline, abandon_date FROM tasks WHERE abandon_date IS NOT NULL ORDER BY abandon_date DESC LIMIT 1 OFFSET " + (i - 1 + current_page * 5);
            IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);
            reader.Read();

            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].name.transform.GetComponent<TMP_Text>().text = reader.GetString(0);
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].deadline.transform.GetComponent<TMP_Text>().text = reader.GetString(1);
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].date.transform.GetComponent<TMP_Text>().text = reader.GetString(2);
            reader.Close(); reader = null;
            i++;
        }

        while (i <= 5)
        {
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].name.transform.GetComponent<TMP_Text>().text = " - ";
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].deadline.transform.GetComponent<TMP_Text>().text = " - ";
            refTasks.transform.GetComponent<Task_References>().tasks[i - 1].date.transform.GetComponent<TMP_Text>().text = " - ";
            i++;
        }
    }

    public void Do_003_Previous_Failed_Page()
    {
        PlayerPrefs.SetInt("Current_Failed_Page_Task", PlayerPrefs.GetInt("Current_Failed_Page_Task") - 1);
        Debug.Log("Current Failed Page Will be: " + PlayerPrefs.GetInt("Current_Failed_Page_Task"));
        Do_003_Action_Open_Failed_Tasks();
    }

    public void Do_003_Next_Failed_Page()
    {
        PlayerPrefs.SetInt("Current_Failed_Page_Task", PlayerPrefs.GetInt("Current_Failed_Page_Task") + 1);
        Debug.Log("Current Failed Page Will be: " + PlayerPrefs.GetInt("Current_Failed_Page_Task"));
        Do_003_Action_Open_Failed_Tasks();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void Do_004_Action_Open_Skills()
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////// T H I S    I S     T H E    P A R T     F O R    A D D I N G     L I S T E N E R S//////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        int i = 0;
        for(i=0; i<5; i++)
        {
            refSkills.transform.GetComponent<Skill_References>().btn_add_time[i].transform.GetComponent<Button>().onClick.RemoveAllListeners();
            refSkills.transform.GetComponent<Skill_References>().btn_add_time[i].transform.GetComponent<Button>()
                .onClick.AddListener(() => Do_004_Action_Add_Time_Click(i));
        }

        refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[0].transform.GetComponent<Button>()
            .onClick.RemoveAllListeners();
        refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[0].transform.GetComponent<Button>()
            .onClick.AddListener(() => Do_004_Previous_Page());
        refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[1].transform.GetComponent<Button>()
            .onClick.RemoveAllListeners();
        refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[1].transform.GetComponent<Button>()
            .onClick.AddListener(() => Do_004_Next_Page());

        i = 1;
        int current_page = PlayerPrefs.GetInt("Current_Page_Skill", 0);

        refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[0].transform.GetComponent<Button>().interactable = true;
        refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[1].transform.GetComponent<Button>().interactable = true;

        string query = "SELECT COUNT(*) FROM skills";
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int table_number); // aici aflam cate elemente are tabelul
        // Debug.Log("There are " + table_number + " elements in the table");
        // Debug.Log("Current page is: " + PlayerPrefs.GetInt("Current_Page_Skill", 0));

        if (current_page < 0)
        {
            current_page = 0;
            PlayerPrefs.SetInt("Current_Page_Skill", current_page);
        }
        if ((current_page + 1) * 5 > table_number)
        {
            current_page = table_number / 5;
            PlayerPrefs.SetInt("Current_Page_Skill", current_page);
        }

        if (table_number <= 5)
        {
            refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[0].transform.GetComponent<Button>().interactable = false;
            refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[1].transform.GetComponent<Button>().interactable = false;
            refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[2].transform.GetComponent<TMP_Text>().text = "1/1";
        }
        else
        {
            if (current_page == 0 && table_number > 5)
                refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[0].transform.GetComponent<Button>().interactable = false;

            if ((current_page + 1) * 5 >= table_number && table_number > 5)
                refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[1].transform.GetComponent<Button>().interactable = false;

            //////////////////////////////////////    Eroare detectata, ai grija ca asa ai facut la toate si asta va creea probleme!
            //////////////////////////////////////    Trebuie sa mergi la toate verificarile de genul de la alte canvasuri si sa te asiguri ca e ok calculat totul
            ///////////////////////////////////////   Altfel daca ai 5 elemente pe pagina si 10 elemente in total o sa-ti arate programul ca sunt 3 pagini!!!!

            string output_page = (current_page + 1) + " / " + (((table_number - 1) / 5) + 1);
            refSkills.transform.GetComponent<Skill_References>().skill_output_page_handler[2].transform.GetComponent<TMP_Text>().text = output_page;
        }

        while (i + current_page * 5 <= table_number && i <= 5)
        {
            /////////////////////////// E L E M E N T S   //////////////////////////////
            query = "SELECT skill_name, skill_level, hours_invested FROM skills WHERE id > 0 ORDER BY place_order ASC LIMIT 1 OFFSET " + (i - 1 + current_page * 5);
            IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);

            reader.Read();
            refSkills.transform.GetComponent<Skill_References>().skill_name[i - 1].transform.GetComponent<TMP_Text>().text = reader.GetString(0);
            refSkills.transform.GetComponent<Skill_References>().skill_level[i - 1].transform.GetComponent<TMP_Text>().text = reader.GetString(1);
            refSkills.transform.GetComponent<Skill_References>().skill_total_time[i - 1].transform.GetComponent<TMP_Text>().text = reader.GetValue(2).ToString() + "  Hours";
            refSkills.transform.GetComponent<Skill_References>().btn_add_time[i - 1].transform.GetComponent<Button>().interactable = true;
            reader.Close(); reader = null;
            // PE AICI AM RAMAS
            i++;
        }

        while (i <= 5)
        {
            refSkills.transform.GetComponent<Skill_References>().skill_name[i - 1].transform.GetComponent<TMP_Text>().text = " - ";
            refSkills.transform.GetComponent<Skill_References>().skill_level[i - 1].transform.GetComponent<TMP_Text>().text = " - ";
            refSkills.transform.GetComponent<Skill_References>().skill_total_time[i - 1].transform.GetComponent<TMP_Text>().text = " - ";
            refSkills.transform.GetComponent<Skill_References>().btn_add_time[i - 1].transform.GetComponent<Button>().interactable = false;
            refSkills.transform.GetComponent<Skill_References>().dd_hours[i - 1].transform.GetComponent<TMP_Dropdown>().interactable = false;
            i++;
        }
    }

    public void Do_004_Action_Add_Time_Click(int skill_number)
    {
        int i = skill_number;
        int current_page = PlayerPrefs.GetInt("Current_Page_Skill", 0);
        float option = refSkills.transform.GetComponent<Skill_References>().dd_hours[i].transform.GetComponent<TMP_Dropdown>().value;
        Debug.Log("The value is: " + option);

        if (option == 1) option = 0.5f;
        else if (option == 0) option = 1f;
        else if (option == 2) option = 0.25f;
        string query = "UPDATE skills SET hours_invested = hours_invested + " + option + " WHERE place_order = " + (i + current_page * 5);
        refObj.transform.GetComponent<References>().executeNonQuery(query);
        Do_004_Action_Open_Skills();
    }

    public void Do_004_Previous_Page()
    {
        PlayerPrefs.SetInt("Current_Page_Skill", (PlayerPrefs.GetInt("Current_Page_Skill") - 1));
        Do_004_Action_Open_Skills();
    }

    public void Do_004_Next_Page()
    {
        PlayerPrefs.SetInt("Current_Page_Skill", (PlayerPrefs.GetInt("Current_Page_Skill") + 1));
        Do_004_Action_Open_Skills();
    }

    public void Do_004_Open_Change_Skill()
    {
        refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[0].transform.GetComponent<Button>().interactable = true;
        refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[1].transform.GetComponent<Button>().interactable = true;
        refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[0].transform.GetComponent<Button>()
            .onClick.RemoveAllListeners();
        refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[0].transform.GetComponent<Button>()
            .onClick.AddListener(() => Do_004_Previous_Skill());
        refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[1].transform.GetComponent<Button>()
            .onClick.RemoveAllListeners();
        refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[1].transform.GetComponent<Button>()
            .onClick.AddListener(() => Do_004_Next_Skill());

        int current_skill = PlayerPrefs.GetInt("Current_Skill_To_Change", 0);
        string query = "SELECT COUNT(*) FROM skills";
        refObj.transform.GetComponent<References>().GetSingleElemFromDB(query, out int total_skills);

        if (current_skill == 0)
        {
            refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[0].transform.GetComponent<Button>().interactable = false;
        }
        if (current_skill + 1 == total_skills)
        {
            refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[1].transform.GetComponent<Button>().interactable = false;
        }
        refSkills.transform.GetComponent<Skill_References>().change_skill_output_page_handler[2].transform.GetComponent<TMP_Text>().text = (current_skill + 1) + "-" + total_skills;

        query = "SELECT skill_name, skill_level, hours_invested, started_learning, enjoyment_level FROM skills WHERE place_order =" + current_skill;
        IDataReader reader = refObj.transform.GetComponent<References>().executeQuery(query);
        reader.Read();
        refSkills.transform.GetComponent<Skill_References>().change_skill_elements[0].transform.GetComponent<TMP_InputField>().text = reader.GetString(0);
        //string[] skill_levels;
        //skill_levels = new string[] {"None", "Novie", "Intermediate", "Advanced", "Expert", "Master"};
        refSkills.transform.GetComponent<Skill_References>().change_skill_elements[1].transform.GetChild(0).transform.GetComponent<TMP_Text>().text = reader.GetString(1);
        refSkills.transform.GetComponent<Skill_References>().change_skill_elements[2].transform.GetComponent<TMP_InputField>().text = reader.GetValue(2).ToString();
        refCalendar.transform.GetComponent<Calendar_References>().calendar_array[6].transform.GetComponent<TMP_Text>().text = reader.GetString(3);
        refSkills.transform.GetComponent<Skill_References>().change_skill_elements[3].transform.GetComponent<TMP_InputField>().text = reader.GetInt32(4).ToString();
        
        refSkills.transform.GetComponent<Skill_References>().apply_skill_changes.transform.GetComponent<Button>()
            .onClick.RemoveAllListeners();
        refSkills.transform.GetComponent<Skill_References>().apply_skill_changes.transform.GetComponent<Button>()
            .onClick.AddListener(() => Do_004_Action_Apply_Change());
    }

    public void Do_004_Previous_Skill()
    {
        PlayerPrefs.SetInt("Current_Skill_To_Change", (PlayerPrefs.GetInt("Current_Skill_To_Change") - 1));
        Do_004_Open_Change_Skill();
    }

    public void Do_004_Next_Skill()
    {
        PlayerPrefs.SetInt("Current_Skill_To_Change", (PlayerPrefs.GetInt("Current_Skill_To_Change") + 1));
        Do_004_Open_Change_Skill();
    }

    public GameObject refObj;
    public void Do_004_Action_Apply_Change()
    {
        int skill_number = PlayerPrefs.GetInt("Current_Skill_To_Change", 0);
        string query = "UPDATE skills SET " +
            "skill_name = '" + refSkills.transform.GetComponent<Skill_References>().change_skill_elements[0].transform.GetComponent<TMP_InputField>().text + "', " +
            "skill_level = '" + refSkills.transform.GetComponent<Skill_References>().change_skill_elements[1].transform.GetChild(0).transform.GetComponent<TMP_Text>().text + "' , " +
            "hours_invested = " + refSkills.transform.GetComponent<Skill_References>().change_skill_elements[2].transform.GetComponent<TMP_InputField>().text + " , " +
            "started_learning = '" + refCalendar.transform.GetComponent<Calendar_References>().calendar_array[6].transform.GetComponent<TMP_Text>().text + "', " +
            "enjoyment_level = " + refSkills.transform.GetComponent<Skill_References>().change_skill_elements[3].transform.GetComponent<TMP_InputField>().text +
            " WHERE place_order =" + skill_number;
        refObj.transform.GetComponent<References>().executeNonQuery(query);

        refObj.transform.GetComponent<References>().counter = -1;
        refObj.transform.GetComponent<References>().Message = " Skill changed succesfully";
    }
}
