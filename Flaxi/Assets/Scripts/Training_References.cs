﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Training_References : MonoBehaviour
{
    public GameObject previous_page;
    public GameObject next_page;
    public GameObject page_shower;
    public GameObject Exercise_Name;
    public GameObject Calories_Per_Exercise;
    public GameObject Enjoyment_Level;
    public GameObject Exercise_Difficulty;
    public GameObject Current_Exercise;
    public GameObject Previous_Exercise;
    public GameObject Next_Exercise;

    // Start is called before the first frame update
    public GameObject[] output_field;
    public GameObject[] output_field1;
    public GameObject[] output_field2;
    public GameObject[] button_click;
    public GameObject[] switch_page;
    public GameObject[] change_exercise_output;
    void Start()
    {
        switch_page = new GameObject[3];
        switch_page[0] = previous_page;
        switch_page[1] = next_page;
        switch_page[2] = page_shower;

        change_exercise_output = new GameObject[7];
        change_exercise_output[0] = Exercise_Name;
        change_exercise_output[1] = Calories_Per_Exercise;
        change_exercise_output[2] = Enjoyment_Level;
        change_exercise_output[3] = Exercise_Difficulty;
        change_exercise_output[4] = Current_Exercise;
        change_exercise_output[5] = Previous_Exercise;
        change_exercise_output[6] = Next_Exercise;
    }
}
