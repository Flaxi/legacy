﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Input_References : MonoBehaviour
{
    public GameObject diary_title;
    public GameObject diary_text;
    public GameObject new_task_input_task_name;
    public GameObject new_task_input_task_deadline;
    public GameObject new_skill_input_skill_name;
    public GameObject new_skill_dropwodn_skill_level;
    public GameObject new_skill_input_skill_total_time;
    public GameObject new_skill_input_start_day;
    public GameObject new_skill_input_start_month;
    public GameObject new_skill_input_start_year;
    public GameObject new_skill_input_enjoyment;

    public GameObject new_job_name;
    public GameObject[] new_job_d;
    public GameObject new_job_daily_hours;
    public GameObject new_job_start_day;
    public GameObject new_job_start_month;
    public GameObject new_job_start_year;
    public GameObject new_job_vacation_days;
    public GameObject new_job_anual_bonus;
    public GameObject new_job_anual_bonus_dropdown_currency;
    public GameObject new_job_salary_amount;
    public GameObject new_job_salary_amount_dropdown_currency;
    public GameObject new_job_salary_amount_dropdown_time_period;
    public GameObject new_job_enjoyment;
    public GameObject new_job_is_main;

    public GameObject new_person_first_name;
    public GameObject new_person_last_name;
    public GameObject new_person_status_int;
    public GameObject new_person_status_name;
    public GameObject new_person_bd_year;
    public GameObject new_person_bd_month;
    public GameObject new_person_bd_day;
    public GameObject new_person_fs_year;
    public GameObject new_person_fs_month;
    public GameObject new_person_fs_day;
    public GameObject new_person_bookmarked;
    public GameObject new_person_favourite_colour;
    public GameObject new_person_job_name;

    public GameObject new_exercise_exercise;
    public GameObject new_exercise_calories_burned_exercise;
    public GameObject new_exercise_enjoyment_level;
    public GameObject new_exercise_difficulty_int;
    
    // Start is called before the first frame update
    public GameObject[] input_field;
    void Start()
    {
        input_field = new GameObject[80];
        input_field[0] = diary_title;
        input_field[1] = diary_text;
        input_field[2] = new_task_input_task_name;
        input_field[3] = new_task_input_task_deadline;
        input_field[4] = new_skill_input_skill_name;
        input_field[5] = new_skill_dropwodn_skill_level;
        input_field[6] = new_skill_input_skill_total_time;
        input_field[7] = new_skill_input_start_day;
        input_field[8] = new_skill_input_start_month;
        input_field[9] = new_skill_input_start_year;
        input_field[10] = new_skill_input_enjoyment;
        input_field[11] = new_job_name;
        input_field[12] = new_job_d[1];
        input_field[13] = new_job_d[2];
        input_field[14] = new_job_d[3];
        input_field[15] = new_job_d[4];
        input_field[16] = new_job_d[5];
        input_field[17] = new_job_d[6];
        input_field[18] = new_job_d[7];
        input_field[19] = new_job_daily_hours;
        input_field[20] = new_job_start_day;
        input_field[21] = new_job_start_month;
        input_field[22] = new_job_start_year;
        input_field[23] = new_job_vacation_days;
        input_field[24] = new_job_anual_bonus;
        input_field[25] = new_job_anual_bonus_dropdown_currency;
        input_field[26] = new_job_salary_amount;
        input_field[27] = new_job_salary_amount_dropdown_currency;
        input_field[28] = new_job_salary_amount_dropdown_time_period;
        input_field[29] = new_job_enjoyment;
        input_field[30] = new_job_is_main;
        input_field[31] = new_person_first_name;
        input_field[32] = new_person_last_name;
        input_field[33] = new_person_status_int;
        input_field[34] = new_person_status_name;
        input_field[35] = new_person_bd_year;
        input_field[36] = new_person_bd_month;
        input_field[37] = new_person_bd_day;
        input_field[38] = new_person_fs_year;
        input_field[39] = new_person_fs_month;
        input_field[40] = new_person_fs_day;
        input_field[41] = new_person_bookmarked;
        input_field[42] = new_person_favourite_colour;
        input_field[43] = new_person_job_name;
        input_field[44] = new_exercise_exercise;
        input_field[45] = new_exercise_calories_burned_exercise;
        input_field[46] = new_exercise_enjoyment_level;
        input_field[47] = new_exercise_difficulty_int;
    }
}
