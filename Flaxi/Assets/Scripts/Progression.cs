﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Progression : MonoBehaviour
{
    [SerializeField] private Sprite circleSprite;
    private RectTransform graphContainer;
    
    public float x_start;
    public float x_end;
    public float y_start;
    public float y_end;

    private float last_x_start;
    private float last_y_start;

    public GameObject scrollerX;
    public GameObject scrollerY;
    public float SizeOfX = 3000;
    public float SizeOfY = 2000;
    public float yMaximum = 0; //was 100f
    // Start is called before the first frame update
    void Start()
    {
        graphContainer = transform.Find("GraphContainer").GetComponent<RectTransform>();

        //CreateCircle(new Vector2(200,200));
        List<int> valueList = new List<int>() {5, 580, 560, 450, 1300, 1220, 1170, 1150, 130, 170, 250, 370, 400, 360, 330 };
        List<int> skipValue = new List<int>() {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        x_start = 0;
        x_end = graphContainer.sizeDelta.x;
        y_start = 0;
        y_end = graphContainer.sizeDelta.y;
        if(yMaximum == 0) yMaximum = Mathf.Max(valueList.ToArray());

        last_x_start = x_start;
        last_y_start = y_start;
        ShowGraph(valueList, skipValue);
    }

    void DeleteGraph(){
        foreach (Transform child in graphContainer.transform) {
            GameObject.Destroy(child.gameObject);
        }
    }

    // Update is called once per frame
    void Update(){
        x_start = Mathf.Round(scrollerX.transform.GetComponent<Scrollbar>().value*100)/100 * SizeOfX;
        //if(x_start > SizeOfX - graphContainer.sizeDelta.x) x_start = SizeOfX - graphContainer.sizeDelta.x;
        x_end = graphContainer.sizeDelta.x + x_start;
        y_start = Mathf.Round(scrollerY.transform.GetComponent<Scrollbar>().value*100)/100 * SizeOfY;
        //if(y_start > SizeOfY - graphContainer.sizeDelta.y) y_start = SizeOfY - graphContainer.sizeDelta.y;
        y_end = graphContainer.sizeDelta.y + y_start;

        List<int> valueList = new List<int>() {5, 580, 560, 450, 1300, 1220, 1170, 1150, 130, 170, 250, 370, 400, 360, 330 };
        List<int> skipValue = new List<int>() {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        if(last_x_start != x_start || last_y_start != y_start){
            DeleteGraph();
            ShowGraph(valueList, skipValue);
            last_x_start = x_start;
            last_y_start = y_start;
        }
    }

    private GameObject CreateCircle(Vector2 anchoredPosition)
    {
        GameObject circle = new GameObject("circle", typeof(Image));
        circle.transform.SetParent(graphContainer, false);
        circle.GetComponent<Image>().sprite = circleSprite;
        circle.GetComponent<Image>().useSpriteMesh = true;
        RectTransform rectTransform = circle.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = anchoredPosition;
        rectTransform.sizeDelta = new Vector2(11, 11);
        rectTransform.anchorMin = new Vector2(0,0);
        rectTransform.anchorMax = new Vector2(0,0);
        return circle;
    }

    private void ShowGraph(List<int> valueList, List<int> skipValue){
        float graphHeight = graphContainer.sizeDelta.y;
        float xSize = 50f; //difference between x values

        GameObject lastCircleGameObject = null;
        for(int i=0; i<valueList.Count; i++){
            float xPosition = i * xSize;
            float yPosition = (valueList[i] / yMaximum) * graphHeight;
            if(skipValue[i] != 1 && x_start <= xPosition && xPosition <= x_end){
                if(x_start > 0) xPosition -= x_start;
                if(y_start > 0) yPosition -= y_start;

                GameObject circleGameObject = CreateCircle(new Vector2(xPosition, yPosition));
                if(lastCircleGameObject != null){
                    CreateDotConnection(lastCircleGameObject.GetComponent<RectTransform>().anchoredPosition, circleGameObject.GetComponent<RectTransform>().anchoredPosition);
                }
                lastCircleGameObject = circleGameObject;
            }
        }
    }

    private void CreateDotConnection(Vector2 dotPositionA, Vector2 dotPositionB){
        GameObject connection = new GameObject("dotConnection", typeof(Image));
        connection.transform.SetParent(graphContainer, false);
        connection.GetComponent<Image>().color = new Color(1,1,1, 0.5f);
        RectTransform rectTransform = connection.GetComponent<RectTransform>();
        Vector2 dir = (dotPositionB - dotPositionA).normalized;
        float distance = Vector2.Distance(dotPositionA, dotPositionB);
        rectTransform.anchorMin = new Vector2(0,0);
        rectTransform.anchorMax = new Vector2(0,0);
        rectTransform.sizeDelta = new Vector2(distance, 3f);
        rectTransform.anchoredPosition = dotPositionA + dir * distance * 0.5f;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        rectTransform.localEulerAngles = new Vector3(0, 0, angle);
    }
}
