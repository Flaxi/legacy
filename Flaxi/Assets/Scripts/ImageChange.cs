﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ImageChange : MonoBehaviour
{
    // public Image thisImage;
    public bool debugON = false;
    public string[] portraits;
    public int i = -1;

    public void Start()
    {
        debugON = false;
        portraits = new string[] { "Sprite0", "Sprite1", "Sprite2", "Sprite3", "Sprite4", "Sprite5" };
        i = PlayerPrefs.GetInt("MyInt", 0);
        gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(portraits[i]);
    }
   
    public void ChangeTheImage()
    {       
        if (i + 1 > 4)
        {
            i = 0;
            PlayerPrefs.SetInt("MyInt", i);
        }
        else
        {
            i++;
            PlayerPrefs.SetInt("MyInt", i);
        }

        if(debugON){
            Debug.Log(i);
            Debug.Log("    ");
            Debug.Log(portraits[i]);
        }
        //victorie trebuia facut un folder numit Resources si acolo trebuia pusa imaginea
        gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(portraits[i]);

        //Sprite ImageOne = Resources.Load<Sprite>("SpriteOne");
        //  thisImage = GetComponent <Image>();
        // Image theImage;
        // theImage = gameObject.GetComponent<Image>();
        // asta de mai jos functioneaza perfect ca sa schimbe culoarea
        // gameObject.GetComponent<Image>().color = new Color32(255, 255, 225, 100);
    }
}
