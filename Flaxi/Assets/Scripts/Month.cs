﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;

public class Month : MonoBehaviour
{
    public Color monthColor;
    public GameObject obj;
    public GameObject[] months;
    public GameObject chosenMonth;
    public GameObject chosenYear;
    //check database limits for low and high
    public int yearLowLimit = 1900;
    public int yearHighLimit = 2100;
    public Button yearLeft;
    public Button yearRight;
    void Start()
    {
        int k = 0;
        months = new GameObject[12];
        for(int i=1; i<=4; i++){
            for(int j=1; j<=3; j++){
                //unity loses scope of loop variable. So if I do MonthColoring(k), k will be 12 for all months
                //with a new variable x for each listener, the parameters are different for each month
                int x = k; 
                months[k] = transform.GetChild(1).GetChild(i-1).GetChild(j-1).gameObject;
                months[k].transform.GetComponent<Button>().onClick.AddListener(() => {MonthColoring(x);});
                k++;
            }
        }
        yearLeft.onClick.AddListener(YearMinus);
        yearRight.onClick.AddListener(YearPlus);
    }

    public void YearMinus(){
        int year = int.Parse(chosenYear.transform.GetComponent<InputField>().text);
        year--;      
        if(year < yearLowLimit) year = yearLowLimit;
        chosenYear.transform.GetComponent<InputField>().text = year.ToString();
    }

    public void YearPlus(){
        int year = int.Parse(chosenYear.transform.GetComponent<InputField>().text);
        year++;
        if(year > yearHighLimit) year = yearHighLimit;
        chosenYear.transform.GetComponent<InputField>().text = year.ToString();
    }

    public void MonthColoring(int monthIndex){
        chosenMonth.transform.GetComponent<InputField>().text = months[monthIndex].transform.GetChild(0).GetComponent<Text>().text;
        chosenMonth.transform.GetChild(2).GetComponent<Text>().text = (monthIndex+1).ToString();
        for (int i=0; i<months.Length; i++){
            if(i == monthIndex) UpdateColor(months[i], Color.green); 
            else UpdateColor(months[i], Color.white);
        }
    }

    // Update is called once per frame
    void Update()
    {
        int year = int.Parse(chosenYear.transform.GetComponent<InputField>().text);
        if(year > 999 && year < 10000){         
            if(year < yearLowLimit) year = yearLowLimit;
            if(year > yearHighLimit) year = yearHighLimit;
            chosenYear.transform.GetComponent<InputField>().text = year.ToString();
        }
        if(year > 9999){
            year = year % 10;
            chosenYear.transform.GetComponent<InputField>().text = year.ToString();
        }
    }

    public void UpdateColor(GameObject obj, Color newColor)
    {
        obj.GetComponent<RawImage>().color = newColor;
        monthColor = newColor;
    }
}
