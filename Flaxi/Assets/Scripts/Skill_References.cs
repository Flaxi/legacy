﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_References : MonoBehaviour
{
    public GameObject skill_previous_page;
    public GameObject skill_next_page;
    public GameObject skill_page_number;

    /// <summary>
    /// ///////////////////////////////// NOW COMES CHANGE SKILL CANVAS
    /// </summary>

    public GameObject change_skill_name;
    public GameObject change_skill_level;
    public GameObject change_skill_total_time;
    public GameObject change_skill_enjoyment_level;

    public GameObject change_skill_previous_page;
    public GameObject change_skill_next_page;
    public GameObject change_skill_page_number;


    public GameObject apply_skill_changes;
    public GameObject event_message_canvas;
    /// //////////////////////////////////////////////////////
    public GameObject[] skill_name;
    public GameObject[] btn_add_time;
    public GameObject[] dd_hours;
    public GameObject[] skill_level;
    public GameObject[] skill_total_time;
    public GameObject[] skill_output_page_handler;
    public GameObject[] change_skill_elements;
    public GameObject[] change_skill_output_page_handler;

    void Start()
    {
        skill_output_page_handler = new GameObject[3];
        skill_output_page_handler[0] = skill_previous_page;
        skill_output_page_handler[1] = skill_next_page;
        skill_output_page_handler[2] = skill_page_number;

        change_skill_elements = new GameObject[4];
        change_skill_elements[0] = change_skill_name;
        change_skill_elements[1] = change_skill_level;
        change_skill_elements[2] = change_skill_total_time;
        change_skill_elements[3] = change_skill_enjoyment_level;

        change_skill_output_page_handler = new GameObject[3];
        change_skill_output_page_handler[0] = change_skill_previous_page;
        change_skill_output_page_handler[1] = change_skill_next_page;
        change_skill_output_page_handler[2] = change_skill_page_number;
    }
}
